<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

/**
 * Get the name of a weekday in finnish
 *
 * @param weekday The number representing the day of the week (0-6)
 * @param strip_consonant Strip double consonants from the name to be
 *                        used in finnish locative cases
 * @return String containing the name of the day in finnish
 */
function utility_get_finnish_weekday($weekday, $strip_consonant = false)
{
	switch ($weekday)
	{
	case 0:
		return "Sunnuntai";
		break;
	case 1:
		return "Maanantai";
		break;
	case 2:
		return "Tiistai";
		break;
	case 3:
		if ($strip_consonant == true)
			return "Keskiviiko";
		else
			return "Keskiviikko";
		break;
	case 4:
		return "Torstai";
		break;
	case 5:
		return "Perjantai";
		break;
	case 6:
		return "Lauantai";
		break;
	default:
		return null;
		break;
	}
}

/**
 * Get the name of a month in finnish
 *
 * @param unixtime A unixtime from which to get the month name
 * @return String containing the name of the month in finnish
 */
function utility_get_finnish_month_name($unixtime)
{
	$date_array = getdate($unixtime);
	switch($date_array["mon"])
	{
	case 1:
		return "Tammikuu";
		break;
	case 2:
		return "Helmikuu";
		break;
	case 3:
		return "Maaliskuu";
		break;
	case 4:
		return "Huhtikuu";
		break;
	case 5:
		return "Toukokuu";
		break;
	case 6:
		return "Kes&auml;kuu";
		break;
	case 7:
		return "Hein&auml;kuu";
		break;
	case 8:
		return "Elokuu";
		break;
	case 9:
		return "Syyskuu";
		break;
	case 10:
		return "Lokakuu";
		break;
	case 11:
		return "Marraskuu";
		break;
	case 12:
		return "Joulukuu";
		break;
	default:
		return null;
		break;
	}
}

function utility_get_finnish_week($unixtime)
{
	$week = date("W", $unixtime);
	return "Viikko ".$week;
}

/**
 * Return the unixtime for the week's monday (i.e. if the $unixtime parameter
 * is a thursday of week 34, this function returns the monday of week 34).
 * It doesn't touch the time at all.
 *
 * @param unixtime Normal unixtime (i.e. seconds since epoch)
 * @return A unixtime representing the monday of the same week
 */
function utility_get_monday_of_week($unixtime)
{
	$date = getdate($unixtime);

	// Go back as many days towards monday as the mday points.
	switch ($date["wday"])
	{
	case 0:
		$monday_unixtime = strtotime("-6 days", $unixtime);
		break;
	case 1:
		$monday_unixtime = $unixtime;
		break;
	case 2:
		$monday_unixtime = strtotime("-1 days", $unixtime);
		break;
	case 3:
		$monday_unixtime = strtotime("-2 days", $unixtime);
		break;
	case 4:
		$monday_unixtime = strtotime("-3 days", $unixtime);
		break;
	case 5:
		$monday_unixtime = strtotime("-4 days", $unixtime);
		break;
	case 6:
		$monday_unixtime = strtotime("-5 days", $unixtime);
		break;
	default:
		$monday_unixtime = $unixtime;
		break;
	}

	// Make sure that the time is 00:00 on monday instead of
	// the time given in $unixtime. This strips the time part away
	$monday_unixtime = strtotime(date("Y-m-d", $monday_unixtime));

	return $monday_unixtime;
}

/*****************************************************************************
 * Page header/footer utilities
 *****************************************************************************/

/**
 * Echo the default page header, title, etc...
 * @param title The page <title> tag contents
 * @param subtitle The page subtitle
 * @param bodyadd Additional stuff to be added to <body> tag
 * @param logged_in If true, displays the logout button and attaches an anchor
 *                  to the home icon. Otherwise, well, doesn't do the above...
 */
function utility_get_default_page_header($title, $subtitle = null, $bodyadd = null, $logged_in = true) {
    $header  = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
    $header .= "<html>\n";
    $header .= "<head>\n";
    $header .= "<title>Tilavaraus - $title</title>\n";
    $header .= "<link href=\"reservation.css\" rel=\"stylesheet\" type=\"text/css\">\n";
    $header .= "<link rel=\"shortcut icon\" href=\"/tilavaraus/favicon.ico\" type=\"image/x-icon\">";
    $header .= "</head>\n";
    $header .= "<body $bodyadd>\n";

    $header .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n";
    $header .= "<tr>\n";

    // Header texts
    $header .= "<td class=\"page_header\">\n";
    if ($logged_in == true) {
        $header .= "<a href=\"weekview.php\">";
    }
    $header .= "<img src=\"home.png\" align=\"absmiddle\" border=\"0\">";
    if ($logged_in == true) {
        $header .= "</a>";
    }
    $header .= DEFAULT_PAGE_HEADER;
    if ($subtitle != null) {
        $header .= " - $subtitle";
    }
    $header .= "</td>\n";

    if ($logged_in == true) {
        $header .= "<td align=\"right\" class=\"page_header\">\n";
        $header .= "<form action=\"logout.php\" method=\"POST\">\n";
        $header .= "<input type=\"submit\" value=\"Kirjaudu ulos\"";
        $header .= "class=\"button\">\n";
        $header .= "</form>\n";
        $header .= "</td>\n";
    }

    $header .= "</tr>\n";
    $header .= "</table>\n";

    $header .= "<br>\n";
    $header .= "<center>\n";

    return $header;
}

/**
 * Echo a page header that redirects to another page
 *
 * @param redirect_to The URL to redirect to (after 1 sec)
 * @param title Page title text (optional)
 * @param subtitle Page subtitle text (optional)
 */
function utility_get_redirect_page_header($redirect_to, $title = null, $subtitle = null) {
    $header  = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
    $header .= "<html>\n";
    $header .= "<head>\n";
    $header .= "<title>$title</title>\n";
    $header .= "<link href=\"reservation.css\" rel=\"stylesheet\" type=\"text/css\">\n";
    $header .= "<meta http-equiv=\"Refresh\" content=\"1;URL=$redirect_to\">";
    $header .= "</head>\n";
    $header .= "<body>\n";

    $header .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n";
    $header .= "<tr>\n";

    // Header texts
    $header .= "<td class=\"page_header\">";
    $header .= "<img src=\"home.png\" align=\"absmiddle\">";
    $header .= DEFAULT_PAGE_HEADER;

    if ($subtitle != null) {
        $header .= " - $subtitle";
    }
    $header .= "</td>\n";

    $header .= "</tr>\n";
    $header .= "</table>\n";

    $header .= "<br>\n";
    $header .= "<center>\n";

    return $header;
}

/**
 * Echo the default page footer
 */
function utility_get_default_page_footer() {
    $footer = "<br>Copyright (c) 2017 <a href='mailto:hjunnila@users.noreply.github.com'>Heikki Junnila</a>\n";
    $footer .= "</center>\n";
    $footer .= "</body>\n";
    $footer .= "</html>\n";

    return $footer;
}

/**
 * Get a generic error message
 *
 * @param title The title of the page
 * @param message The message to display
 */
function utility_get_fail_message($title, $message = null) {
    $msg  = "<font class=\"failed_header\">".$title."</font>\n";
    $msg .= "<br>\n<br>\n";

    if ($message != null) {
        $msg .= "<font class=\"failed_message\">$message</font>\n";
        $msg .= "<br>\n<br>\n";
    }

    $msg .= "<form>\n";
    $msg .= "<input type=\"button\" value=\"Takaisin\" ";
    $msg .= "onClick=\"javascript: history.go(-1)\" class=\"button\">\n";
    $msg .= "</form>\n";

    return $msg;
}

/**
 * Get a generic success message
 *
 * @param title The title of the page
 * @param message An optional message to display
 */
function utility_get_success_message($title, $message = null) {
    $msg  = "<font class=\"success_header\">".$title."</font>\n";
    $msg .= "<br>\n<br>\n";

    if ($message != null) {
        $msg .="<font class=\"success_message\">$message</font>\n";
        $msg .= "<br>\n<br>\n";
    }

    $msg .= "<form action=\"weekview.php\" method=\"POST\">\n";
    $msg .= "<input type=\"submit\" value=\"Takaisin viikkon&auml;kym&auml;&auml;n\" class=\"button\">\n";
    $msg .= "</form>\n";

    return $msg;
}

/**
 * Get all HTTP headers from the current client request
 *
 * @return The request headers as a string
 */
function utility_get_client_headers() {
    return implode("\r\n", getallheaders());
}

/**
 * Attempt to extract the client's IP address
 *
 * @return The client IP address or 0.0.0.0 if not available
 */
function utility_get_client_ip() {
    // Get client's IP address
    if (isset($_SERVER['HTTP_CLIENT_IP']) && array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $ips = array_map('trim', $ips);
        $ip = $ips[0];
    } else {
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = '0.0.0.0';
        }
    }

    $ip = filter_var($ip, FILTER_VALIDATE_IP);
    $ip = ($ip === false) ? '0.0.0.0' : $ip;

    return $ip;
}

/**
 * Create a cryptographic salt
 */
function utility_create_salt() {
    return uniqid(mt_rand(), true);
}

?>
