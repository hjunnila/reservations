#!/bin/mysql
#
# Copyright (c) 2017 Heikki Junnila
# See COPYING for licensing information.
#
# This is a sample script to create a table for the room reservation system. If you wish to change the table names
# you should also make similar changes to database.inc.php or otherwise the system will not find correct tables.
#
# The script can be run thru command line with the mysql shell:
#     $ mysql <database name> -u [username] -p < activity.sql > output.txt
#
# Another (perhaps more preferable) way to execute the script is to use the import function in phpMyAdmin.

#
# Create a table for user activity tracking
#
CREATE TABLE IF NOT EXISTS `activity` (
    `id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `username` TEXT ( 254 ) NOT NULL ,
    `timestamp` DATETIME NOT NULL ,
    `event` TEXT ,
    `client_headers` TEXT ,
    `client_ip` TEXT
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
