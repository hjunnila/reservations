#!/bin/mysql
#
# Copyright (c) 2017 Heikki Junnila
# See COPYING for licensing information.
#
# This is a sample script to create a table for the room reservation system. If you wish to change the table names
# you should also make similar changes to database.inc.php or otherwise the system will not find correct tables.
#
# The script can be run thru command line with the mysql shell:
#     $ mysql <database name> -u [username] -p < users.sql > output.txt
#
# Another (perhaps more preferable) way to execute the script is to use the import function in phpMyAdmin.

#
# Create a table for users
#
CREATE TABLE IF NOT EXISTS `users` (
    `username` VARCHAR ( 254 ) NOT NULL PRIMARY KEY ,
    `password` TEXT NOT NULL ,    
    `salt` TEXT NOT NULL ,
    `admin` INTEGER( 1 ) NOT NULL DEFAULT '0' ,
    `realname` TEXT NOT NULL ,
    `telephone` TEXT NULL ,
    `email` TEXT NULL
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
