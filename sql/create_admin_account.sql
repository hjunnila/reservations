#!/bin/mysql
#
# Copyright (c) 2017 Heikki Junnila
# See COPYING for licensing information.
#
# This is a sample script to create a table for the room reservation system. If you wish to change the table names
# you should also make similar changes to database.inc.php or otherwise the system will not find correct tables.
#
# The script can be run thru command line with the mysql shell:
#     $ mysql ohsrk_res -u [username] -p < create_admin_account.sql > output.txt
#
# Another (perhaps more preferable) way to execute the script is to use the import function in phpMyAdmin.

#
# Insert the first admin user and its password hash. The password is "secret".
#
INSERT INTO `users` (username,password,admin,realname,telephone,email) VALUES (
    "admin" ,
    "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4" ,
    "1" ,
    "Teh Administratorz" ,
    "+358 40 1234 567" ,
    "foo@bar.com"
);
