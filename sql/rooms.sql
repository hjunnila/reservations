#!/bin/mysql
#
# Copyright (c) 2017 Heikki Junnila
# See COPYING for licensing information.
#
# This is a sample script to create a table for the room reservation system. If you wish to change the table names
# you should also make similar changes to database.inc.php or otherwise the system will not find correct tables.
#
# The script can be run thru command line with the mysql shell:
#     $ mysql <database name> -u [username] -p < rooms.sql > output.txt
#
# Another (perhaps more preferable) way to execute the script is to use the import function in phpMyAdmin.

#
# Create a table for available rooms
#
CREATE TABLE IF NOT EXISTS `rooms` (
    `id` TINYINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `name` TEXT NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
