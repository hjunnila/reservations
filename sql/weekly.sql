#!/bin/mysql
#
# Copyright (c) 2017 Heikki Junnila
# See COPYING for licensing information.
#
# This is a sample script to create a table for the room reservation system. If you wish to change the table names
# you should also make similar changes to database.inc.php or otherwise the system will not find correct tables.
#
# The script can be run thru command line with the mysql shell:
#     $ mysql <database name> -u [username] -p < weekly.sql > output.txt
#
# Another (perhaps more preferable) way to execute the script is to use the import function in phpMyAdmin.

#
# Create a table for weekly reservations
#
CREATE TABLE IF NOT EXISTS `weekly` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `room` INT NOT NULL ,
    `reservee` TEXT NOT NULL ,
    `purpose` TEXT NOT NULL ,
    `description` TEXT NULL ,
    `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    `weekday` TINYINT NOT NULL ,
    `start` TINYINT NOT NULL ,
    `end` TINYINT NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

