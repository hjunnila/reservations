<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "user.inc.php";
include "room.inc.php";
include "reservation.inc.php";

// Rooms
define("TABLE_ROOMS", "rooms");
define("TABLE_ROOMS_ID", "id");
define("TABLE_ROOMS_NAME", "name");

// Users
define("TABLE_USERS", "users");
define("TABLE_USERS_REALNAME", "realname");
define("TABLE_USERS_ADMIN", "admin");
define("TABLE_USERS_USERNAME", "username");
define("TABLE_USERS_PASSWORD", "password");
define("TABLE_USERS_SALT", "salt");
define("TABLE_USERS_TELEPHONE", "telephone");
define("TABLE_USERS_EMAIL", "email");

// Reservations
define("TABLE_RESERVATIONS", "reservations");
define("TABLE_RESERVATIONS_ID", "id");
define("TABLE_RESERVATIONS_ROOM", "room");
define("TABLE_RESERVATIONS_DATE", "date");
define("TABLE_RESERVATIONS_RESERVEE", "reservee");
define("TABLE_RESERVATIONS_PURPOSE", "purpose");
define("TABLE_RESERVATIONS_DESCRIPTION", "description");
define("TABLE_RESERVATIONS_TIMESTAMP", "timestamp");
define("TABLE_RESERVATIONS_START", "start");
define("TABLE_RESERVATIONS_END", "end");

// Weekly reservations
define("TABLE_WEEKLY", "weekly");
define("TABLE_WEEKLY_ID", "id");
define("TABLE_WEEKLY_ROOM", "room");
define("TABLE_WEEKLY_WEEKDAY", "weekday");
define("TABLE_WEEKLY_RESERVEE", "reservee");
define("TABLE_WEEKLY_PURPOSE", "purpose");
define("TABLE_WEEKLY_DESCRIPTION", "description");
define("TABLE_WEEKLY_TIMESTAMP", "timestamp");
define("TABLE_WEEKLY_START", "start");
define("TABLE_WEEKLY_END", "end");

// Activity tracking
define("TABLE_ACTIVITY", "activity");
define("TABLE_ACTIVITY_USERNAME", "username");
define("TABLE_ACTIVITY_TIMESTAMP", "timestamp");
define("TABLE_ACTIVITY_EVENT", "event");
define("TABLE_ACTIVITY_CLIENT_HEADERS", "client_headers");
define("TABLE_ACTIVITY_CLIENT_IP", "client_ip");

class Database {
    private $conn = null;

    function open() {
        $this->conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

        if ($this->conn->connect_errno) {
            throw new Exception("Connection error: ".$this->conn->connect_errno);
        }
    }

    function begin() {
        if (!$this->conn->begin_transaction()) {
            throw new Exception("Unable to start transaction: ".$this->conn.connect_error);
        }
    }

    function select_for_update($table) {
        if (!$this->conn->query("select * from $table for update;")) {
            throw new Exception("Unable to select ".$table." for update: ".$this->conn->connect_error);
        }
    }

    function rollback() {
        if (!$this->conn->rollback()) {
            throw new Exception("Unable to perform rollback: ".$this->conn.connect_error);
        }
    }

    function commit() {
        if (!$this->conn->commit()) {
            throw new Exception("Unable to perform commit: ".$this->conn.connect_error);
        }
    }

    function close() {
        if (!$this->conn->close()) {
            throw new Exception("Unable to close database connection: ".$this->conn.connect_error);
        }
    }

    /**
     * Check, whether the username with the given password are found from user table.
     * The password is hashed and salted before DB comparison.
     *
     * @param username The username to check for
     * @param password The password to check for (in clear text)
     * @return "user" for regular users, "admin" for admin users, null == invalid user.
     */
    function authenticate_user($username, $password) {
        if ($username == null || $password == null) {
            return null;
        }

        $user_type = null;

        // Query for the user with a prepared query
        if ($query = $this->conn->prepare("SELECT ".TABLE_USERS_USERNAME.",".TABLE_USERS_PASSWORD.",".TABLE_USERS_SALT.",".TABLE_USERS_ADMIN." FROM ".TABLE_USERS." WHERE ".TABLE_USERS_USERNAME." = ?")) {
            $query->bind_param("s", $username);
        } else {
            throw new Exception("Unable to prepare query");
        }
        
        $query->execute();
        $result = $query->get_result();
 
        // Exactly one row should be returned if the username & password match
        if ($result) {
            if ($result->num_rows == 1) {
                // Double-check that the returned row is for the queried user
                $row = $result->fetch_array(MYSQLI_ASSOC);
                if ($row[TABLE_USERS_USERNAME] != $username) {
                    throw new Exception("Database returned invalid data");
                }

                $db_pwd = $row[TABLE_USERS_PASSWORD];
                $salt = $row[TABLE_USERS_SALT];
                $hash = sha1($password);

                if ($salt == null || strlen($salt) == 0) {
                    error_log("Not a salted/peppered $username");
                    // Not a salted & peppered password entry. Need to migrate if password itself matches.
                    if ($db_pwd == $hash) {
                        error_log("Migrating user $username");
                        $this->set_user_password($username, $password);
                    } else {
                        // Wrong unsalted/peppered password
                        error_log("Invalid password for $username");
                        return null;
                    }
                } else {
                    $salted_hash = sha1($hash.PASSWORD_PEPPER.$salt);

                    if ($salted_hash != $db_pwd) {
                        // Wrong password
                        error_log("Invalid salted & peppered password for $username");
                        return null;
                    }
                }

                // Check, whether user is admin and set $admin_status
                if ($row[TABLE_USERS_ADMIN] == 1) {
                    $user_type = "admin";
        		} else {
        		    $user_type = "user";
        		}
            }

            $result->close();
        }

        return $user_type;
    }

    /**
     * Get user information
     *
     * @param username The username to query
     * @return An object of type User or null if an error occurred
     */
    function get_user_info($username) {
        if ($username == null) {
            return FALSE;
        }

        $user = (object) null;

        if ($query = $this->conn->prepare("SELECT ".TABLE_USERS_USERNAME.",".TABLE_USERS_REALNAME.",".TABLE_USERS_TELEPHONE.",".TABLE_USERS_EMAIL.",".TABLE_USERS_ADMIN." FROM ".TABLE_USERS." WHERE username = ?")) {
            $query->bind_param("s", $username);
        } else {
            throw new Exception("Unable to prepare query");
        }
        
        $query->execute();
        $result = $query->get_result();

        // Query for the username
        if ($result) {
            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();
                $user = new User;
                $user->username = $row[TABLE_USERS_USERNAME];
                $user->realname = $row[TABLE_USERS_REALNAME];
                $user->admin = $row[TABLE_USERS_ADMIN];
                $user->telephone = $row[TABLE_USERS_TELEPHONE];
                $user->email = $row[TABLE_USERS_EMAIL];
            }

            $result->close();
        }

        return $user;
    }

    /**
     * Get a list of users in the system
     *
     * @return An array of User objects
     */
    function get_user_list() {
        if ($query = $this->conn->prepare("SELECT ".TABLE_USERS_USERNAME.",".TABLE_USERS_REALNAME.",".TABLE_USERS_EMAIL.",".TABLE_USERS_TELEPHONE.",".TABLE_USERS_ADMIN." FROM ".TABLE_USERS.";")) {
            // NOP
        } else {
            throw new Exception("Unable to prepare query");
        }
        
        $query->execute();
        $result = $query->get_result();

        $user_list = array();
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $user = new User;
                $user->username = $row[TABLE_USERS_USERNAME];
                $user->realname = $row[TABLE_USERS_REALNAME];
                $user->email = $row[TABLE_USERS_EMAIL];
                $user->telephone = $row[TABLE_USERS_TELEPHONE];
                $user->admin = $row[TABLE_USERS_ADMIN];
                $user_list[] = $user;
            }

            $result->close();
        }

        return $user_list;
    }

    /**
     * Remove a user from the database
     *
     * @param username The username of the user entry to delete
     * @return TRUE for success, FALSE for errors
     */
    function remove_user($username) {
        if (!$username) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("DELETE FROM ".TABLE_USERS." WHERE ".TABLE_USERS_USERNAME." = ?")) {
            $query->bind_param("s", $username);
        } else {
            throw new Exception("Unable to prepare query");
        }
        
        $this->begin();
        $this->select_for_update(TABLE_USERS);
        $query->execute();

        if ($this->conn->errno) {
            error_log("remove user error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Create a new user entry into users table. Hashes and salts the password.
     *
     * @param user The user info to insert
     * @param password The user's password (in clear text)
     *
     * @return TRUE on success; FALSE on failure
     */
    function create_user($user, $password) {
        if (!$user || !$password) {
            return FALSE;
        }

        $hashed = sha1($password);
        $salt = utility_create_salt();
        $salted_hash = sha1($hashed.PASSWORD_PEPPER.$salt);

        if ($query = $this->conn->prepare("INSERT INTO ".TABLE_USERS
                ." (".TABLE_USERS_USERNAME.",".TABLE_USERS_REALNAME.",".TABLE_USERS_TELEPHONE
                .",".TABLE_USERS_EMAIL.",".TABLE_USERS_PASSWORD.",".TABLE_USERS_SALT.",".TABLE_USERS_ADMIN.")"
                ." VALUES (?,?,?,?,?,?,?)")) {
            $admin = ($user->admin) ? 1 : 0;
            $query->bind_param("ssssssi", $user->username, $user->realname, $user->telephone, $user->email, $salted_hash, $salt, $admin);
        } else {
            throw new Exception("Unable to prepare query");
        }

        // Perform the insertion
        $this->begin();
        $this->select_for_update(TABLE_USERS);
        $query->execute();

        if ($this->conn->errno) {
            error_log("create user error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Change a user's information. $user->username is used as the user's identifier; all other data
     * in the database entry is replaced with data from the given $user object, except for password.
     *
     * @param user The user, whose info to change.
     * @return TRUE if the update was successful, otherwise FALSE
     */
    function change_user_info($user) {
        if ($user == null || $user->username == null) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("UPDATE ".TABLE_USERS
                ." SET ".TABLE_USERS_REALNAME." = ?, ".TABLE_USERS_TELEPHONE." = ?, "
                .TABLE_USERS_EMAIL." = ?, ".TABLE_USERS_ADMIN." = ? WHERE "
                .TABLE_USERS_USERNAME." = ?")) {
            $admin = ($user->admin) ? 1 : 0;
            $query->bind_param("sssis", $user->realname, $user->telephone, $user->email, $admin, $user->username);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $this->begin();
        $this->select_for_update(TABLE_USERS);
        $query->execute();

        if ($this->conn->errno) {
            error_log("change user info error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Set a user's password. The password is hashed before it is inserted to database.
     *
     * @param username The username, whose password to change
     * @param password The new password to set for the user (in clear text)
     * @return TRUE if the update was successful, otherwise FALSE
     */
    function set_user_password($username, $password) {
        if (!$username || !$password) {
            return FALSE;
        }

        $hashed = sha1($password);
        $salt = utility_create_salt();
        $salted_hash = sha1($hashed.PASSWORD_PEPPER.$salt);

        if ($query = $this->conn->prepare("UPDATE ".TABLE_USERS
                ." SET ".TABLE_USERS_PASSWORD." = ?, ".TABLE_USERS_SALT." = ?"
                ." WHERE ".TABLE_USERS_USERNAME." = ?")) {
            $query->bind_param("sss", $salted_hash, $salt, $username);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $this->begin();
        $this->select_for_update(TABLE_USERS);
        $query->execute();

        if ($this->conn->errno) {
            error_log("set user password error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Log an event for a user activity.
     *
     * @param user The user, whose logon to track
     * @param event The event that occurred (login, logout, ...)
     * @param headers The client's headers
     * @param ip The client's originating IP
     * @return TRUE if the update was successful, otherwise FALSE
     */
    function track_user_activity($username, $event, $headers, $ip) {
        if ($query = $this->conn->prepare("INSERT INTO ".TABLE_ACTIVITY.
                 " (".TABLE_ACTIVITY_USERNAME.
                 ",".TABLE_ACTIVITY_TIMESTAMP.
                 ",".TABLE_ACTIVITY_EVENT.
                 ",".TABLE_ACTIVITY_CLIENT_HEADERS.
                 ",".TABLE_ACTIVITY_CLIENT_IP.
                 ") VALUES (?, ?, ?, ?, ?);")) {
            $timestamp = date("Y-m-d H:i:s", time());
            $query->bind_param("sssss", $username, $timestamp, $event, $headers, $ip);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $this->begin();
        $this->select_for_update(TABLE_ACTIVITY);
        $query->execute();

        if ($this->conn->errno) {
            error_log("track user activity error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Get a list of rooms
     *
     * @return An array of Room objects
     */
    function get_rooms() {
        // Query all rooms.
        if ($query = $this->conn->prepare("SELECT ".TABLE_ROOMS_ID.",".TABLE_ROOMS_NAME." FROM ".TABLE_ROOMS.";")) {
            // NOP
        } else {
            throw new Exception("Unable to prepare query");
        }

        $query->execute();
        $result = $query->get_result();
        $rooms = array();

        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $room = new Room;
                $room->id = $row[TABLE_ROOMS_ID];
                $room->name = $row[TABLE_ROOMS_NAME];
                $rooms[] = $room;
            }

            $result->close();
        }

        return $rooms;
    }

    /**
     * Get a Room object by its id
     *
     * @param room_id The ID of the room
     * @return A Room object or null if not found
     */
    function get_room($room_id) {
        if (!$room_id) {
            return null;
        }

        $room = (object) null;

        // Query all room data for given room id
        if ($query = $this->conn->prepare("SELECT * FROM ".TABLE_ROOMS." WHERE ".TABLE_ROOMS_ID." = ? ;")) {
            $query->bind_param("s", $room_id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $query->execute();
        $result = $query->get_result();

        if ($result) {
            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();
                $room = new Room;
                $room->id = $row[TABLE_ROOMS_ID];
	            $room->name = $row[TABLE_ROOMS_NAME];
            }

            $result->close();
        }

        return $room;
    }

    /**
     * Create a new room entry into users table.
     *
     * @param room The Room to insert
     *
     * @return TRUE on success; FALSE on failure
     */
    function create_room($room) {
        if (!$room || strlen($room->name) == 0) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("INSERT INTO ".TABLE_ROOMS." (".TABLE_ROOMS_NAME.") VALUES (?);")) {
            $query->bind_param("s", $room->name);
        } else {
            throw new Exception("Unable to prepare query");
        }

        // Perform the insertion
        $this->begin();
        $this->select_for_update(TABLE_ROOMS);
        $query->execute();

        if ($this->conn->errno) {
            error_log("create room error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Change a room's information. $user->id is used as the user's identifier; all other data
     * in the database entry is replaced with data from the given $room object.
     *
     * @param room The room, whose info to change.
     * @return TRUE if the update was successful, otherwise FALSE
     */
    function change_room_info($room) {
        if ($room == null || $room->id == null) {
            return FALSE;
        }

        // Update room info
        if ($query = $this->conn->prepare("UPDATE ".TABLE_ROOMS." SET ".TABLE_ROOMS_NAME." = ?"." WHERE ".TABLE_ROOMS_ID." = ? ;")) {
            $query->bind_param("si", $room->name, $room->id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $this->begin();
        $this->select_for_update(TABLE_ROOMS);
        $query->execute();

        if ($this->conn->errno) {
            error_log("change room info error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Remove a room from the database
     *
     * @param room_id The id of the room to delete
     * @return TRUE for success, FALSE for errors
     */
    function remove_room($room_id) {
        if (!$room_id) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("DELETE FROM ".TABLE_ROOMS." WHERE ".TABLE_ROOMS_ID." = ? ;")) {
            $query->bind_param("i", $room_id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $this->begin();
        $this->select_for_update(TABLE_ROOMS);
        $query->execute();

        if ($this->conn->errno) {
            error_log("remove room error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Query a week's worth of reservations for the given $room, starting
     * from $unixtime date. Returns an array of arrays of reservation entries (array ( array (id, room, start, end...) ) )
     *
     * @param room The room ID under inspection
     * @param unixtime The monday of the current week in unixtime
     * @return An array of arrays or null in case of errors. The returned top-level array could be null if there are no reservations.
     */
    function get_reservations($room_id, $unixtime) {
        if (!$room_id || !$unixtime) {
            return null;
        }

        $array = array();

        if ($query = $this->conn->prepare("SELECT * FROM ".TABLE_RESERVATIONS." WHERE ".TABLE_RESERVATIONS_ROOM." = ? "."AND "
                                          .TABLE_RESERVATIONS_DATE." >= ? AND ".TABLE_RESERVATIONS_DATE." <= ? ;")) {
            // TODO: Some sense to this unixtime madness!!!!!
            // Convert dates to be compatible with database query
            $from_date_db = date("Y-m-d", $unixtime);
            $to_date_db = date("Y-m-d", strtotime("+6 days", $unixtime));
            $query->bind_param("iss", $room_id, $from_date_db, $to_date_db);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $query->execute();
        $result = $query->get_result();
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $array[] = $row;
            }

            $result->close();
        }

        return $array;
    }

    /**
     * Query for weekly recurring reservations for the given $room
     *
     * @param db_link An open database connection
     * @param room The room ID under inspection
     * @param unixtime The monday of the current week in unixtime
     */
    function get_weekly_reservations($room_id) {
        if (!$room_id) {
            return null;
        }

        $array = array();

        if ($query = $this->conn->prepare("SELECT * FROM ".TABLE_WEEKLY." WHERE ".TABLE_WEEKLY_ROOM." = ? ;")) {
            $query->bind_param("i", $room_id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $query->execute();
        $result = $query->get_result();
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $array[] = $row;
            }

            $result->close();
        }

        return $array;
    }

    /**
     * Get details either for a single reservation or a weekly reservation.
     *
     * @param res_id The ID of the reservation
     * @param type The type of the reservation (weekly or single)
     * @return A Reservation object or null
     */
    function get_reservation_details_by_type($res_id, $type) {
        if ($type == RESERVATION_TYPE_SINGLE) {
            return $this->get_reservation_details($res_id);
        } else if ($type == RESERVATION_TYPE_WEEKLY) {
            return $this->get_weekly_details($res_id);
        } else {
            return null;
        }
    }

    /**
     * Get details for one reservation.
     *
     * @param res_id The ID of the reservation
     * @return A Reservation object or null
     */
    function get_reservation_details($res_id) {
        if (!$res_id) {
            return null;
        }

        $reservation = (object) null;

        if ($query = $this->conn->prepare("SELECT * FROM ".TABLE_RESERVATIONS.",".TABLE_ROOMS." WHERE ".TABLE_RESERVATIONS.".".TABLE_RESERVATIONS_ID." = ? "
                                          ."AND ".TABLE_ROOMS.".".TABLE_ROOMS_ID."=".TABLE_RESERVATIONS.".room;")) {
            $query->bind_param("i", $res_id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $query->execute();
        $result = $query->get_result();

        if ($result && $result->num_rows == 1) {
            $row = $result->fetch_assoc();

            $reservation = new Reservation;
            $reservation->id = $res_id;
            $reservation->type = RESERVATION_TYPE_SINGLE;

            $reservee = $this->get_user_info($row[TABLE_RESERVATIONS_RESERVEE]);
            if (!$reservee) {
                // Reservee doesn't exist. Fabricate the user info.
                $reservee = new User;
                $reservee->username = $reservee;
                $reservee->realname = $reservee;
                $reservee->telephone = "Tuntematon";
                $reservee->email = "Tuntematon";
                $reservee->admin = FALSE;
            }

            $reservation->reservee = $reservee;
            $reservation->purpose = $row[TABLE_RESERVATIONS_PURPOSE];
            $reservation->description = $row[TABLE_RESERVATIONS_DESCRIPTION];
            $reservation->date = $row[TABLE_RESERVATIONS_DATE];
            $reservation->timestamp = $row[TABLE_RESERVATIONS_TIMESTAMP];
            $reservation->start = $row[TABLE_RESERVATIONS_START];
            $reservation->end = $row[TABLE_RESERVATIONS_END];
            $reservation->room_id = $row[TABLE_ROOMS_ID]; // Table join makes room id appear as "id"
            $reservation->room_name = $row[TABLE_ROOMS_NAME]; // Table join makes room name appear as "name"

            $result->close();
        }

        return $reservation;
    }

    /**
     * Get details for one weekly reservation.
     *
     * @param weekly_id The ID of the weekly reservation
     * @return A Reservation object or null
     */
    function get_weekly_details($weekly_id) {
        if ($weekly_id == null) {
            return null;
        }

        $weekly = (object) null;

        if ($query = $this->conn->prepare("SELECT * FROM ".TABLE_WEEKLY." WHERE ".TABLE_WEEKLY_ID." = ? ;")) {
            $query->bind_param("i", $weekly_id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        $query->execute();
        $result = $query->get_result();

        if ($result && $result->num_rows == 1) {
            $row = $result->fetch_assoc();

            $reservation = new Reservation;
            $reservation->id = $weekly_id;
            $reservation->type = RESERVATION_TYPE_WEEKLY;

            $reservee = $this->get_user_info($row[TABLE_WEEKLY_RESERVEE]);
            if (!$reservee) {
                // Reservee doesn't exist. Fabricate the user info.
                $reservee = new User;
                $reservee->username = $reservee;
                $reservee->realname = $reservee;
                $reservee->telephone = "Tuntematon";
                $reservee->email = "Tuntematon";
                $reservee->admin = FALSE;
            }

            $reservation->reservee = $reservee;
            $reservation->purpose = $row[TABLE_WEEKLY_PURPOSE];
            $reservation->description = $row[TABLE_WEEKLY_DESCRIPTION];
            $reservation->weekday = $row[TABLE_WEEKLY_WEEKDAY];
            $reservation->timestamp = $row[TABLE_WEEKLY_TIMESTAMP];
            $reservation->start = $row[TABLE_WEEKLY_START];
            $reservation->end = $row[TABLE_WEEKLY_END];
            $reservation->room_id = $row[TABLE_WEEKLY_ROOM];
            $room = $this->get_room($reservation->room_id);
            $reservation->room_name = $room->name;

            $result->close();
        }

        return $reservation;
    }

    /**
     * Check, whether a room is free at a given date and time. If the reservation object has an id
     * it is ignored in the query to prevent it from being an overlapping occurrence to itself.
     *
     * Returns 0 if the room is free, otherwise returns the SQL result
     * In error cases, returns -1
     *
     * @param reservation The reservation to check against.
     * @param ignore_reservation_id Exclude this reservation from the query (optional)
     * @return 0 if there are no overlaps, -1 for error and >0 for num of overlaps
     */
    function get_overlapping_reservations($reservation) {
        if ($reservation == null || $reservation->room_id == null || $reservation->date == null || 
            $reservation->start == null || $reservation->end == null) {
            return -1;
        }

        $query_str = "SELECT * FROM ".TABLE_RESERVATIONS;
        $query_str .= " WHERE date = ? AND room = ?";
        if ($reservation->id != null) {
            // Ignore the current reservation id in the query
            $query_str .= " AND id <> ?";
        }
        // Reservation attempts that start within an existing reservation
        $query_str .= " AND ((start <= ? AND end > ?) ";
        // Reservation attempts that start before but end within an existin reservation
        $query_str .= "OR (start < ? AND end >= ?) ";
        // Reservations that start & end within the new attempt's timeframe
        $query_str .= "OR (start >= ? AND end <= ?))"; 

        if ($query = $this->conn->prepare($query_str)) {
            if ($reservation->id != null) {
                $query->bind_param("siissssss", $reservation->date, $reservation->room_id, $reservation->id,
                                   $reservation->start, $reservation->start,
                                   $reservation->end, $reservation->end,
                                   $reservation->start, $reservation->end);
            } else {
                $query->bind_param("sissssss", $reservation->date, $reservation->room_id,
                                   $reservation->start, $reservation->start,
                                   $reservation->end, $reservation->end,
                                   $reservation->start, $reservation->end);
            }
        } else {
            throw new Exception("Unable to prepare query");
        }

        $query->execute();
        $result = $query->get_result();

        // Check for errors
        $errno = $this->conn->errno;
        if ($errno != 0) {
            echo $this->conn->errno.": ".$this->conn->error."\n";
            return $errno;
        } else {
            if ($result != null) {
                $num_overlaps = $result->num_rows;
                return $num_overlaps;
            } else {
                return -1;
            }
        }
    }

    /**
     * Check from weekly reservations, whether a room is free at a given date and time.
     *
     * Returns 0 if the room is free, otherwise returns the SQL result
     * In error cases, returns -1
     *
     * @param reservation The reservation to check against
     * @param ignore_weekly_id ID of a weekly reservation to ignore (optional)
     * @return 0 if there are no overlaps, -1 for error and >0 for overlaps
     */
    function get_overlapping_weekly($reservation, $ignore_weekly_id = null) {
        if ($reservation == null || $reservation->room_id == null || $reservation->date == null ||
            $reservation->start == null || $reservation->end == null) {
            return -1;
        }

        // Query overlapping reservations for the given date & room
        $query_str = "SELECT * FROM ".TABLE_WEEKLY." WHERE ".TABLE_WEEKLY_ROOM." = ? AND ".TABLE_WEEKLY_WEEKDAY." = ?";
        if ($ignore_weekly_id != null) {
            $query_str .= " AND ".TABLE_WEEKLY_ID." <> ?";
        }
        // Reservation attempts that start within an existing reservation
        $query_str .= " AND ((start <= ? AND end > ?)";
        // Reservation attempts that
        $query_str .= " OR (start < ? AND end >= ?)";
        // Reservations that start & end within the new attempt's timeframe
        $query_str .= " OR (start >= ? AND end <= ?))";

        if ($query = $this->conn->prepare($query_str)) {
            $unixtime = strtotime($reservation->date);
            $date_array = getdate($unixtime);
            $weekday = $date_array["wday"];
            if ($ignore_weekly_id != null) {
                $query->bind_param("iiissssss", $reservation->room_id, $weekday,
                                   $ignore_weekly_id,
                                   $reservation->start, $reservation->start,
                                   $reservation->end, $reservation->end,
                                   $reservation->start, $reservation->end);
            } else {
                $query->bind_param("iissssss", $reservation->room_id, $weekday,
                                   $reservation->start, $reservation->start,
                                   $reservation->end, $reservation->end,
                                   $reservation->start, $reservation->end);
            }
        } else {
            throw new Exception("Unable to prepare query");
        }

        $query->execute();
        $result = $query->get_result();

        // Check for errors
        $errno = $this->conn->errno;
        if ($errno != 0) {
            echo $this->conn->errno.": ".$this->conn->error."\n";
            return $errno;
        } else {
            if ($result != null) {
                $num_overlaps = $result->num_rows;
                return $num_overlaps;
            } else {
                return -1;
            }
        }
    }

    /**
     * Insert a new reservation row to database
     *
     * @param reservation The reservation to insert
     * @return TRUE if successful; otherwise FALSE
     */
    function create_reservation($reservation) {
        if ($reservation->type == RESERVATION_TYPE_SINGLE) {
            return $this->create_reservation_single($reservation);
        } else if ($reservation->type == RESERVATION_TYPE_WEEKLY) {
            return $this->create_reservation_weekly($reservation);
        } else {
            return null;
        }
    }

    /**
     * Insert a new single reservation row to database
     *
     * @param reservation The reservation to insert
     * @return TRUE if successful; otherwise FALSE
     */
    private function create_reservation_single($reservation) {
        if ($reservation == null || $reservation->room_id == null ||
            $reservation->date == null || $reservation->start == null || $reservation->end == null ||
	        $reservation->purpose == null || $reservation->reservee == null || $reservation->reservee->username == null) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("INSERT INTO ".TABLE_RESERVATIONS." ("
                                          .TABLE_RESERVATIONS_ROOM.","
                                          .TABLE_RESERVATIONS_RESERVEE.","
                                          .TABLE_RESERVATIONS_PURPOSE.","
                                          .TABLE_RESERVATIONS_DESCRIPTION.","
                                          .TABLE_RESERVATIONS_TIMESTAMP.","
                                          .TABLE_RESERVATIONS_DATE.","
                                          .TABLE_RESERVATIONS_START.","
                                          .TABLE_RESERVATIONS_END.")"
                                          ." VALUES(?,?,?,?,?,?,?,?);")) {
            $timestamp = date("Y-m-d H:i:s", time());
            $query->bind_param("isssssii", $reservation->room_id, $reservation->reservee->username,
                                $reservation->purpose, $reservation->description, $timestamp,
                                $reservation->date, $reservation->start, $reservation->end);
        } else {
            throw new Exception("Unable to prepare query");
        }

        // Begin a transaction
        $this->begin();

        // Select the table for update
        $this->select_for_update(TABLE_RESERVATIONS);

        // Insert the row
        $query->execute();
        if ($this->conn->errno) {
            error_log("create single reservation error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Insert a new weekly reservation row to database
     *
     * @param reservation The reservation to insert
     * @return TRUE if successful; otherwise FALSE
     */
    private function create_reservation_weekly($reservation) {
        if ($reservation == null || $reservation->room_id == null ||
            $reservation->weekday == null || $reservation->start == null || $reservation->end == null ||
	        $reservation->purpose == null || $reservation->reservee == null || $reservation->reservee->username == null) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("INSERT INTO ".TABLE_WEEKLY." ("
                                          .TABLE_WEEKLY_ROOM.","
                                          .TABLE_WEEKLY_RESERVEE.","
                                          .TABLE_WEEKLY_PURPOSE.","
                                          .TABLE_WEEKLY_DESCRIPTION.","
                                          .TABLE_WEEKLY_TIMESTAMP.","
                                          .TABLE_WEEKLY_WEEKDAY.","
                                          .TABLE_WEEKLY_START.","
                                          .TABLE_WEEKLY_END.")"
                                          ." VALUES(?,?,?,?,?,?,?,?);")) {
            $timestamp = date("Y-m-d H:i:s", time());
            $query->bind_param("isssssii", $reservation->room_id, $reservation->reservee->username,
                                $reservation->purpose, $reservation->description, $timestamp,
                                $reservation->weekday, $reservation->start, $reservation->end);
        } else {
            throw new Exception("Unable to prepare query");
        }

        // Begin a transaction
        $this->begin();

        // Select the table for update
        $this->select_for_update(TABLE_WEEKLY);

        // Insert the row
        $query->execute();
        if ($this->conn->errno) {
            error_log("create weekly reservation error: ".$this->conn->error);
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Update some details of an existing reservation, be it either single or weekly.
     *
     * @param reservation The reservation to update.
     * @return TRUE if successful; otherwise FALSE
     */
    function update_reservation($reservation) {
        if ($reservation->type == RESERVATION_TYPE_SINGLE) {
            return $this->update_reservation_single($reservation);
        } else if ($reservation->type == RESERVATION_TYPE_WEEKLY) {
            return $this->update_reservation_weekly($reservation);
        } else {
            return null;
        }
    }

    /**
     * Update some details of an existing single reservation. Only the start & end times, purpose and
     * description can be updated. Moving a reservation to another day is not possible without
     * removing the existing one and making a new one.
     *
     * This function does not begin() a transaction because overlaps should be checked
     * inside the same transaction, too. Therefore, it is up to the caller to handle the transaction.
     *
     * @param reservation The reservation to update.
     * @return TRUE if successful; otherwise FALSE
     */
    private function update_reservation_single($reservation) {
        if ($reservation == null || $reservation->id == null ||
            $reservation->start == null || $reservation->end == null ||
            $reservation->purpose == null) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("UPDATE ".TABLE_RESERVATIONS." SET "
                                          .TABLE_RESERVATIONS_PURPOSE." = ?, "
                                          .TABLE_RESERVATIONS_DESCRIPTION." = ?, "
                                          .TABLE_RESERVATIONS_START." = ?, "
                                          .TABLE_RESERVATIONS_END." = ? "
                                          ." WHERE ".TABLE_RESERVATIONS_ID." = ?;")) {
            $query->bind_param("ssssi", $reservation->purpose, $reservation->description,
                                $reservation->start, $reservation->end, $reservation->id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        // Select the table for update
        $this->select_for_update(TABLE_RESERVATIONS);

        // Update the row
        $query->execute();
        if ($this->conn->errno) {
            error_log("update single reservation error: ".$this->conn->error);
            return FALSE;
        } else {
            return TRUE;
        }
	}

	/**
     * Update some details of an existing weekly reservation. Only the start & end times, purpose and
     * description can be updated. Moving a weekly reservation to another day is not possible without
     * removing the existing one and making a new one.
     *
     * This function does not begin() a transaction because overlaps should be checked
     * inside the same transaction, too. Therefore, it is up to the caller to handle the transaction.
     *
     * @param reservation The reservation to update.
     * @return TRUE if successful; otherwise FALSE
     */
    private function update_reservation_weekly($reservation) {
        if ($reservation == null || $reservation->id == null ||
            $reservation->start == null || $reservation->end == null ||
            $reservation->purpose == null) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("UPDATE ".TABLE_WEEKLY." SET "
                                          .TABLE_WEEKLY_PURPOSE." = ?, "
                                          .TABLE_WEEKLY_DESCRIPTION." = ?, "
                                          .TABLE_WEEKLY_START." = ?, "
                                          .TABLE_WEEKLY_END." = ? "
                                          ." WHERE ".TABLE_WEEKLY_ID." = ?;")) {
            $query->bind_param("ssssi", $reservation->purpose, $reservation->description,
                                $reservation->start, $reservation->end, $reservation->id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        // Select the table for update
        $this->select_for_update(TABLE_WEEKLY);

        // Update the row
        $query->execute();
        if ($this->conn->errno) {
            error_log("update weekly reservation error: ".$this->conn->error);
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Remove the given reservation from database. Initiates and completes inside a transaction.
     *
     * @param reservation The reservation to remove
     * @return TRUE if successful; otherwise FALSE
     */
    function remove_reservation($reservation) {
          if ($reservation->type == RESERVATION_TYPE_SINGLE) {
            return $this->remove_reservation_single($reservation->id);
        } else if ($reservation->type == RESERVATION_TYPE_WEEKLY) {
            return $this->remove_reservation_weekly($reservation->id);
        } else {
            return null;
        }
    }

    /**
     * Remove the given single reservation from database. Initiates and completes inside a transaction.
     *
     * @param res_id The unique id of the reservation to remove
     * @return TRUE if successful; otherwise FALSE
     */
    private function remove_reservation_single($res_id) {
        if ($res_id == null) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("DELETE FROM ".TABLE_RESERVATIONS." WHERE ".TABLE_RESERVATIONS_ID." = ? ;")) {
            $query->bind_param("i", $res_id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        // Begin a transaction
        $this->begin();

        // Select the table for update
        $this->select_for_update(TABLE_RESERVATIONS);

        // Delete the row
        $query->execute();
        if ($this->conn->errno) {
            error_log("remove single reservation error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }

    /**
     * Remove the given weekly reservation from database. Initiates and completes inside a transaction.
     *
     * @param res_id The unique id of the reservation to remove
     * @return TRUE if successful; otherwise FALSE
     */
    private function remove_reservation_weekly($res_id) {
        if ($res_id == null) {
            return FALSE;
        }

        if ($query = $this->conn->prepare("DELETE FROM ".TABLE_WEEKLY." WHERE ".TABLE_WEEKLY_ID." = ? ;")) {
            $query->bind_param("i", $res_id);
        } else {
            throw new Exception("Unable to prepare query");
        }

        // Begin a transaction
        $this->begin();

        // Select the table for update
        $this->select_for_update(TABLE_WEEKLY);

        // Delete the row
        $query->execute();
        if ($this->conn->errno) {
            error_log("remove weekly reservation error: ".$this->conn->error);
            $this->rollback();
            return FALSE;
        } else {
            $this->commit();
            return TRUE;
        }
    }
}

?>
