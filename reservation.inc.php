<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

define("RESERVATION_TYPE_SINGLE", "single");
define("RESERVATION_TYPE_WEEKLY", "weekly");
define("RESERVATION_METHOD_CREATE", "create");
define("RESERVATION_METHOD_UPDATE", "update");

class Reservation {
    var $id;        // Unique id of the reservation
    var $type;          // Type of the reservation (normal or weekly)
    var $reservee;      // A User object representing the reservee

    var $purpose;       // The reservation's purpose string
    var $description;   // The reservation's detailed description

    var $date;          // The occurrence date of the reservation (not used when $type == weekly)
    var $weekday;       // When $type == weekly, contains the weekday when this reservation occurs

    var $timestamp;     // The timestamp when the reservation was made
    var $start;         // Starting hour on the date of occurrence
    var $end;           // Ending hour on the date of occurrence

    var $room_id;       // Unique id of the room where the reservation is made for
    var $room_name;     // Name of the room where the reservation is made for

    /**
     * Check, whether the start and end times make sense (doesn't start before it ends and both
     * are within global limits, are numeric in the first place etc.)
     */
    function check_times() {
        if (!is_numeric($this->start) || !is_numeric($this->end) ||
            $this->end > RESERVATION_LAST_HOUR || $this->end < RESERVATION_FIRST_HOUR ||
            $this->start > RESERVATION_LAST_HOUR || $this->start < RESERVATION_FIRST_HOUR ||
            $this->end <= $this->start) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Get the reservation details view. $current_user is used to determine, whether it is useful
     * to place editing/removal buttons in the view.
     *
     * @param current_user The current user object
     * @return A string containing the reservation details view
     */
    function get_details_view($current_user) {
        // Midnight (00:00) is saved as "24" so this is a safe calculation
        $duration = $this->end - $this->start;

        $unixtime = strtotime($this->date);
        $date_array = getdate($unixtime);

        // Start constructing the view
        $view  = "<table cols=\"2\" width=\"300\" rules=\"cols\" cellspacing=\"0\" cellpadding=\"0\">\n";
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Tila</td>\n";
        $view .= "<td class=\"entry_data\"><b>".$this->room_name."</b></td>\n";
        $view .= "</tr>";

        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Tilaisuus</td>\n";
        $view .= "<td class=\"entry_data\">".$this->purpose."</td>\n";
        $view .= "</tr>";

        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">P&auml;iv&auml;</td>\n";
        $view .= "<td class=\"entry_data\">";
	    if ($this->type == RESERVATION_TYPE_SINGLE) {
	        // Normal single-shot reservation
            $date_array = getdate($unixtime);
            $view .= utility_get_finnish_weekday($date_array["wday"]);
            $view .= date(" d.m.Y", $unixtime);
        } else {
            // Weekly recurring reservation
            $view .= utility_get_finnish_weekday($this->weekday)."sin";
        }
        $view .=  "</td>\n";
        $view .= "</tr>";

        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Alkaa</td>\n";
        $view .= "<td class=\"entry_data\">".$this->start.":00</td>\n";
        $view .= "</tr>";

        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">P&auml;&auml;ttyy</td>\n";
        $view .= "<td class=\"entry_data\">".$this->end.":00</td>\n";
        $view .= "</tr>";

        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Kesto</td>\n";
        $view .= "<td class=\"entry_data\">".$duration."h";
        $view .= "</td>\n";
        $view .= "</tr>";

        $view .= "</table>\n";

        $view .= "<br>";

        $view .= "<table cols=\"2\" width=\"300\" rules=\"cols\" cellspacing=\"0\" cellpadding=\"0\">\n";
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Varaaja</td>\n";
        $view .= "<td class=\"entry_data\"><b>".$this->reservee->realname."</b></td>\n";
        $view .= "</tr>";

        // Show reservee details only for logged-in users to prevent misuse (by automated bots, for example)
        if ($current_user != null) {
            $view .= "<tr>\n";
            $view .= "<td class=\"entry_header\">";
            $view .= "Puhelin</td>\n";
            $view .= "<td class=\"entry_data\">".$this->reservee->telephone."</td>\n";
            $view .= "</tr>";

            $view .= "<tr>\n";
            $view .= "<td class=\"entry_header\">";
            $view .= "S&auml;hk&ouml;posti</td>\n";
            $view .= "<td class=\"entry_data\">";
            $view .= "<a href=\"mailto:".$this->reservee->email."\">".$this->reservee->email."</a></td>\n";
            $view .= "</tr>";
        }

        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Varaus&nbsp;tehty</td>\n";
        $view .= "<td class=\"entry_data\">";
        $view .= utility_get_finnish_weekday($date_array["wday"])."na";
        $view .= date(" d.m.Y", strtotime($this->timestamp))."</td>\n";
        $view .= "</tr>";

        // Description
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Lis&auml;tietoja:</td>\n";
        $view .= "<td class=\"entry_data_hl\">";
        $view .= $this->description."</td>\n";
        $view .= "</tr>";
        $view .= "</table>";

        $view .= "<hr>\n";

        // Control buttons
        $view .= "<table border=\"0\" width=\"300\">\n";
        $view .= "<tr>\n";

        // Show control buttons only for logged-in users
        if ($current_user != null) {
            // Only own reservations can be edited while admins can edit all reservations
            if ($this->reservee->username == $current_user->username || $current_user->admin) {
                $view .= "<td align=\"left\">\n";
                $view .= "<form action=\"remove_reservation.php\" method=\"POST\">\n";
                $view .= "<input type=\"hidden\" name=\"reservation_id\" value=\"$this->id\">\n";
                $view .= "<input type=\"hidden\" name=\"type\" value=\"$this->type\">\n";
                $view .= "<input type=\"submit\" value=\"Poista\" class=\"button\">\n";
                $view .= "</form>\n";
                $view .= "</td>\n";

                $view .= "<td align=\"center\">\n";
                $view .= "<form action=\"edit_reservation.php\" method=\"GET\">\n";
                $view .= "<input type=\"hidden\" name=\"reservation_id\" value=\"$this->id\">\n";
                $view .= "<input type=\"hidden\" name=\"type\" value=\"$this->type\">\n";
                $view .= "<input type=\"submit\" value=\"Muokkaa\" class=\"button\">\n";
                $view .= "</form>\n";
                $view .= "</td>\n";
            }
        }

        $view .= "<td align=\"right\">\n";
        $view .= "<form action=\"weekview.php\" method=\"GET\">\n";
        $view .= "<input type=\"submit\" value=\"Takaisin\" class=\"button\">\n";
        $view .= "</form>\n";
        $view .= "</td>\n";

        $view .= "</tr>\n";
        $view .= "</table>\n";

        return $view;
    }

    /**
     * Return an editor for the reservation
     *
     * @param method The method for the editor ("update" existing one or "create" a new one)
     * @return A string containing the reservation details view
     */
    function get_editor($method, $user = null) {
        if ($method == RESERVATION_METHOD_UPDATE) {
            $posturi = "update_reservation.php";
        } else if ($method == RESERVATION_METHOD_CREATE) {
            $posturi = "create_reservation.php";
        } else {
            echo "Unrecognized method:".$method;
            die();
        }

        $editor  = "<script>function focus_purpose() {\n";
        $editor .= "document.editor.purpose.focus(); }</script>\n";

        $editor .= "<form action=\"".$posturi."\" method=\"POST\" name=\"editor\">\n";
        $editor .= "<input type=\"hidden\" name=\"reservation_id\" value=\"$this->id\"/>";
        $editor .= "<input type=\"hidden\" name=\"type\" value=\"$this->type\"/>";
        $editor .= "<input type=\"hidden\" name=\"room\" value=\"$this->room_id\"/>";
        $editor .= "<input type=\"hidden\" name=\"date\" value=\"$this->date\"/>";
        $editor .= "<input type=\"hidden\" name=\"weekday\" value=\"$this->weekday\"/>";
        $editor .= "<table width=\"400\" cellpadding=\"0\" cellspacing=\"0\">\n";

        // Room
        $editor .= "<tr>\n";
        $editor .= "<td class=\"entry_header\">Tila</td>\n";
        $editor .= "<td class=\"entry_data\">";
        $editor .= $this->room_name."</td>\n";
        $editor .= "</tr>";

        // Date
        $editor .= "<tr>\n";
        $editor .= "<td class=\"entry_header\">P&auml;iv&auml;</td>\n";
        $editor .= "<td class=\"entry_data\">";
        if ($this->type == RESERVATION_TYPE_SINGLE) {
	        // Normal single-shot reservation
            $unixtime = strtotime($this->date);
            $date_array = getdate($unixtime);
            $date_string = utility_get_finnish_weekday($date_array["wday"]);
            $date_string .= date(" d.m.Y", $unixtime);
            $editor .= $date_string;
        } else {
            // Weekly recurring reservation
            $editor .= utility_get_finnish_weekday($this->weekday)."sin";
        }
            
        $editor .= "</td>\n";
        $editor .= "</tr>";

        // Reservee
        $editor .= "<tr>\n";
        $editor .= "<td class=\"entry_header\">Varaaja</td>\n";
        $editor .= "<td class=\"entry_data\">";
        $editor .= $this->reservee->realname."</td>\n";
        $editor .= "</tr>";

        // Start
        $editor .= "<tr>\n";
        $editor .= "<td class=\"entry_header\">Alkaa</td>\n";
        $editor .= "<td class=\"entry_data\">";
        $editor .= "<select name=\"start\">\n";
        for ($i = RESERVATION_FIRST_HOUR; $i < RESERVATION_LAST_HOUR; $i++) {
            $editor .= "<option value=\"$i\"";
            if ($this->start == $i) {
                $editor .= " selected>$i:00</option>\n";
            } else {
                $editor .= ">$i:00</option>\n";
            }
        }
        $editor .= "</select>\n";
        $editor .= "</td>\n";
        $editor .= "</tr>";

        // End
        if ($this->end == null) {
            // End time was not given, default is a 1 hour reservation
            $this->end = $this->start + 1;
        }
        $editor .= "<tr>\n";
        $editor .= "<td class=\"entry_header\">P&auml;&auml;ttyy</td>\n";
        $editor .= "<td class=\"entry_data\">";
        $editor .= "<select name=\"end\">\n";
        for ($i = RESERVATION_FIRST_HOUR + 1; $i <= RESERVATION_LAST_HOUR; $i++) {
            $editor .= "<option value=\"$i\"";
            if ($this->end == $i) {
                $editor .= " selected>$i:00</option>\n";
            } else {
                $editor .= ">$i:00</option>\n";
            }
        }
        $editor .= "</select>\n";
        $editor .= "</td>\n";
        $editor .= "</tr>";

        // Recurrence
        if ($user && $user->admin && $method == RESERVATION_METHOD_CREATE) {
            $editor .= "<tr>\n";
            $editor .= "<td class=\"entry_header\">Toistuu</td>\n";
            $editor .= "<td class=\"entry_data\"><select name=\"recurrence\">";
            $editor .= "<option value=\"".RESERVATION_TYPE_SINGLE."\"" 
                        . (($this->type == RESERVATION_TYPE_SINGLE) ? "selected" : "") . ">Kerran</option>\n";
            $editor .= "<option value=\"".RESERVATION_TYPE_WEEKLY."\""
                        . (($this->type == RESERVATION_TYPE_WEEKLY) ? "selected" : "") . ">Viikoittain</option>\n";
            $editor .= "</td>\n";
            $editor .= "</tr>";
        }

        // Purpose
        $editor .= "<tr>\n";
        $editor .= "<td class=\"entry_header\">K&auml;ytt&ouml;tarkoitus</td>\n";
        $editor .= "<td class=\"entry_data\"><input name=\"purpose\" type=\"text\" size=\"10\" value=\"$this->purpose\"></td>\n";
        $editor .= "</tr>";

        // Description
        $editor .= "<tr>\n";
        $editor .= "<td class=\"entry_header\" valign=\"top\">Lis&auml;tietoja</td>\n";
        $editor .= "<td class=\"entry_data\"><textarea name=\"description\" type=\"text\" rows=\"5\" cols=\"39\">$this->description</textarea></td>\n";
        $editor .= "</tr>";
        $editor .= "</table>\n";

        $editor .= "<hr>\n";
        $editor .= "<input type=\"submit\" value=\"OK\" class=\"button\">";
        $editor .= "<input type=\"button\" value=\"Peruuta\" onClick=\"javascript: history.go(-1)\" class=\"button\">\n";

        $editor .= "</form>";

        return $editor;
    }
}

?>
