<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

class User {
    var $username;
    var $realname;
    var $admin;
    var $telephone;
    var $email;

    /**
     * Get a user editor form for the instance.
     *
     * @param new_user If TRUE, the editor is used to create a new user
     * @param admin TRUE if the editing user is an admin, otherwise FALSE
     */
    function get_editor($new_user, $admin) {
        if ($admin && $new_user) {
            $view = "<form action=\"create_user.php\" method=\"post\" name=\"userinfo\">\n";
        } else {
            $view = "<form action=\"change_user_info.php\" method=\"post\" name=\"userinfo\">\n";
        }

        $view .= "<table width=\"300\" rules=\"none\" cellspacing=\"0\" cellpadding=\"0\">\n";

        // Username
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">K&auml;ytt&auml;j&auml;tunnus</td>\n";
        $view .= "<td class=\"entry_data\">\n";

        // Admin can edit the username only when creating a new user
        if ($new_user && $admin) {
            $view .= "<input type=\"text\" name=\"username\">\n";
        } else {
            $view .= "<input type=\"hidden\" name=\"username\" value=\"$this->username\">\n";
            $view .= "<b>".$this->username."</b>\n";
        }

        $view .= "</td>\n";
        $view .= "</tr>\n";

        // Realname
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Nimi</td>\n";
        $view .= "<td class=\"entry_data\">\n";
        $view .= "<input type=\"text\" value=\"$this->realname\" name=\"realname\">\n";
        $view .= "</td>\n";
        $view .= "</tr>\n";

        // Telephone
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Puhelin</td>\n";
        $view .= "<td class=\"entry_data\">\n";
        $view .= "<input type=\"text\" value=\"$this->telephone\" name=\"telephone\">\n";
        $view .= "</td>\n";
        $view .= "</tr>\n";

        // Email
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">S&auml;hk&ouml;posti</td>\n";
        $view .= "<td class=\"entry_data\">\n";
        $view .= "<input type=\"text\" value=\"$this->email\" name=\"email\">\n";
        $view .= "</td>\n";
        $view .= "</tr>\n";

        // Admin can see the admin status checkbox
        if ($admin) {
            $view .= "<tr>\n";
            $view .= "<td class=\"entry_header\">P&auml;&auml;k&auml;ytt&auml;j&auml;</td>\n";
            $view .= "<td class=\"entry_data\">\n";
            if ($this->admin) {
                $view .= "<input type=\"checkbox\" name=\"admin\" checked>\n";
            } else {
                $view .= "<input type=\"checkbox\" name=\"admin\">\n";
            }
            $view .= "</td>\n";
            $view .= "</tr>\n";
        }

        // Split the table into two parts when editing an existing user's info
        if (!$new_user) {
            // Update userinfo button
            $view .= "<tr>\n";
            $view .= "<td>&nbsp;</td>\n";
            $view .= "<td class=\"entry_data\">\n";
            $view .= "<input type=\"submit\" value=\"Talleta tiedot\" class=\"button\">\n";
            $view .= "</td>\n";
            $view .= "</tr>\n";

            $view .= "</table>\n";
            $view .= "</form>\n";

            $view .= "<br>\n";

            // Password entries
            $view .= "<form action=\"change_password.php\" method=\"POST\" name=\"password\">\n";
            $view .= "<table width=\"300\" rules=\"none\" cellspacing=\"0\" cellpadding=\"0\">\n";

            // Old password
            $view .= "<tr>\n";
            $view .= "<td class=\"entry_header\">Nykyinen salasana</td>\n";
            $view .= "<td class=\"entry_data\">\n";
            $view .= "<input type=\"password\" name=\"oldpassword\">\n";
            $view .= "</td>\n";
            $view .= "</tr>\n";
        }

        // New password 1st
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Uusi salasana</td>\n";
        $view .= "<td class=\"entry_data\">\n";
        $view .= "<input type=\"password\" name=\"newpassword1\">\n";
        $view .= "</td>\n";
        $view .= "</tr>\n";

        // New password 2nd
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Uusi salasana (uudelleen)</td>\n";
        $view .= "<td class=\"entry_data\">\n";
        $view .= "<input type=\"password\" name=\"newpassword2\">\n";
        $view .= "</td>\n";
        $view .= "</tr>\n";

        $view .= "<tr>\n";
        $view .= "<td>&nbsp;</td>\n";
        $view .= "<td class=\"entry_data\">\n";

        if ($new_user) {
            // Create user button
            $view .= "<input type=\"submit\" value=\"Luo\" class=\"button\">\n";
        } else {
            // Change password button
            $view .= "<input type=\"hidden\" name=\"username\" value=\"$this->username\">\n";
            $view .= "<input type=\"submit\" value=\"Vaihda salasana\" class=\"button\">\n";
        }

        $view .= "</td>\n";
        $view .= "</tr>\n";
        $view .= "</table>\n";

        $view .= "</form>\n";

        return $view;
    }

    /**
     * Get the user list view
     *
     * @param user_list An array of users
     * @return A string containing the user editor form
     */
    static function get_list_view($users) {
        $view = "<script type=\"text/javascript\">";
        $view .= "function confirm_user_removal() { var sel = document.UserControlForm.username; var option = sel.options[sel.selectedIndex]; return confirm('Poistetaanko käyttäjä: ' + option.text + '?'); }";
        $view .= "</script>\n";
        $view .= "<table width=\"300\">\n";
        $view .= "<form action=\"edit_user.php\" method=\"POST\" name=\"UserControlForm\">\n";
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_data\">\n";
        $view .= User::get_select_structure($users);
        $view .= "</td>\n";
        $view .= "<td valign=\"top\">\n";
        $view .= "<input type=\"submit\" name=\"edit\" value=\"Muokkaa\" class=\"button\">\n";
        $view .= "<br>\n";
        $view .= "<input type=\"submit\" name=\"remove\" value=\"Poista\" class=\"button\" onClick=\"return confirm_user_removal()\">\n";
        $view .= "<br>\n";
        $view .= "<input type=\"submit\" name=\"new\" value=\"Uusi\" class=\"button\">\n";
        $view .= "</td>\n";
        $view .= "</tr>\n";
        $view .= "</form>\n";
        $view .= "</table>\n";
        $view .= "<hr>\n";
        $view .= "<form action=\"weekview.php\" method=\"POST\">\n";
        $view .= "<input type=\"submit\" value=\"Takaisin viikkon&auml;kym&auml;&auml;n\" class=\"button\">\n";
        $view .= "</form>\n";

        return $view;
    }

    /**
     * Insert the given array of users into a <select> structure and return the resulting HTML
     *
     * @param users An array of users to be inserted into a form as options
     * @return A <select> structure containing the given users
     */
    static function get_select_structure($users) {
        $list = "<select size=\"20\" name=\"username\">\n";
        foreach ($users as $user) {
            $list .= "<option value=\"".$user->username."\">";
            $list .= $user->username ." (".$user->realname.")</option>\n";
        }

        $list .= "</select>\n";

        return $list;
    }
}

?>
