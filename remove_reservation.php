<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "utility.inc.php";
include "token.inc.php";
include "database.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
    // We are not (properly) logged in, redirect to index.php
    echo utility_get_redirect_page_header("index.php");
    echo utility_get_default_page_footer();
    die();
}

// Get basic user info
$username = Token::get_username($jwt);
$admin_status = Token::get_admin($jwt);

// Get the reservation id and its type
$res_id = $_POST['reservation_id'];
$type = $_POST['type'];

echo utility_get_default_page_header("Varauksen poisto");

// Get the reservation from database
$db = new Database;
$db->open();
$reservation = $db->get_reservation_details_by_type($res_id, $type);
if (!$reservation) {
    // Reservation not found.
    echo utility_get_fail_message("Varausta ei l&ouml;ydy.");
    echo utility_get_default_page_footer();
} else if ($admin_status || $reservation->reservee->username == $username) {
    // Reservation was found and we have authority to remove it.
    if ($db->remove_reservation($reservation)) {
        echo utility_get_success_message("Varaus poistettu!");
    } else {
        echo utility_get_fail_message("Varausta ei voida poistaa!");
    }
} else {
    // No authority to remove the reservation.
    echo utility_get_fail_message("Voit poistaa vain omia varauksiasi!");
}

$db->close();

echo utility_get_default_page_footer();

?>
