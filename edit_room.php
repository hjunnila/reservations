<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "token.inc.php";
include "utility.inc.php";
include "database.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not (properly) logged in, redirect to index.php
	echo utility_get_redirect_page_header("index.php");
	echo utility_get_default_page_footer();
	die();
} else if (!Token::get_admin($jwt)) {
	// The user is not an admin, redirect to weekview
    // Only admins are allowed to edit rooms
	echo utility_get_redirect_page_header("weekview.php");
	echo utility_get_default_page_footer();
	die();
}

if (isset($_POST['remove'])) {
    echo utility_get_default_page_header("Poista huone", "Poista huone");

    $room_id = $_POST['room_id'];

    $db = new Database;
    $db->open();
    $result = $db->remove_room($room_id);
    $db->close();

    if ($result) {
        echo utility_get_success_message("Huone poistettu.");
    } else {
        echo utility_get_fail_message("Huoneen poisto ep&auml;onnistui!");
    }
} else if (isset($_POST['new'])) {
    echo utility_get_default_page_header("Luo uusi huone", "Luo uusi huone");
    $room = new Room;
    echo $room->get_editor(TRUE);
} else {
    echo utility_get_default_page_header("Muokkaa huoneen tietoja", "Muokkaa huoneen tietoja");

    $room_id = $_POST['room_id'];

    $db = new Database;
    $db->open();
    $room = $db->get_room($room_id);
    $db->close();

    if ($room) {
        echo $room->get_editor();
    } else {
        echo utility_get_fail_message("Huoneen tietoja ei l&ouml;ydy!");
    }

    // Return button
    echo "<hr>\n";
    echo "<form action=\"room_list.php\" method=\"GET\">\n";
    echo "<input type=\"submit\" value=\"Takaisin huonelistaan\" class=\"button\">\n";
    echo "</form>\n";
}

echo utility_get_default_page_footer();

?>
