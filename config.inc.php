<?php
/*
  Copyright 2018 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

// Change to false if anonymous browsing is not allowed
define("ANONYMOUS_BROWSE_ENABLED", true);

// Minimum password length
define("PASSWORD_MIN_LENGTH", 4);

// This can be anything within 0 < x < last_hour
define("RESERVATION_FIRST_HOUR", 7);

// This can be anything within 0 < x > first_hour.
// Use 24 if you want midnight, NOT zero!
define("RESERVATION_LAST_HOUR", 24);

// Default title used in all page headers
define("DEFAULT_PAGE_HEADER", "Tilavaraus");

// Default time in seconds when our access tokens expire
// 60 seconds per minute, 60 minutes per hour, 24 hours per day
define("TOKEN_TIME", 60 * 60 * 24);

// Default time in seconds when our cookies expire
// 60 seconds per minute, 60 minutes per hour, 24 hours per day
define("COOKIE_TIME", (60 * 60 * 24));

// The location of the private RSA key file (make sure this cannot be accessed thru your webserver)
define("PEM_FILE", "/home/hjunnila/cert/id_rsa_jwt.pem");

// The location of the public RSA key file (make sure this cannot be accessed thru your webserver)
define("PUB_FILE", "/home/hjunnila/cert/id_rsa_jwt.pub");

// The token credentials issuer. Change to your own site name.
define("CREDENTIALS_ISSUER", "https://localhost");

// The hostname that contains the database
define("DB_HOST", "localhost");

// The database user's username
define("DB_USERNAME", "ohsrk_res");

// The database user's password
define("DB_PASSWORD", "secret");

// The name of the database that contains the reservation tables
define("DB_NAME", "ohsrk_res");

/// The pepper used with the password hash + salt (any random, lengthy string that doesn't change)
define("PASSWORD_PEPPER", "9fjdkldep5fhfry37849eufo93754u7wshtr73oqg");

////////////////////////////////////////////////////////////////////////////////////////////////////
// No need to touch these
////////////////////////////////////////////////////////////////////////////////////////////////////

// Name of the authentication token cookie
define("COOKIE_TOKEN", "ohsrk_res_token");

// Name of the currently selected room cookie
define("COOKIE_ROOM", "ohsrk_res_room");

// Name of the currently inspected time cookie
define("COOKIE_UNIXTIME", "ohsrk_res_unixtime");

?>
