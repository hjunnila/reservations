<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "token.inc.php";
include "utility.inc.php";
include "database.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not (properly) logged in, redirect to index.php
	echo utility_get_redirect_page_header("index.php");
	echo utility_get_default_page_footer();
	die();
}

// Default is to edit the token holder's own account
$username = Token::get_username($jwt);
$edit_username = $username;

// Check if current token is for an admin
$admin = FALSE;
if (Token::get_admin($jwt)) {
    $admin = TRUE;

    if (isset($_POST['username'])) {
        // Admin can edit also other users' data. Take the username from POST data when it's given.
        $edit_username = $_POST['username'];
    }
}

if ($admin) {
    // Only admins are allowed to add & remove users
    if (isset($_POST['remove'])) {
        echo utility_get_default_page_header("Poista k&auml;ytt&auml;j&auml;", "Poista k&auml;ytt&auml;j&auml;");

        if ($edit_username != $username) {
            $db = new Database;
            $db->open();
            $result = $db->remove_user($edit_username);
            $db->close();

            if ($result) {
                echo utility_get_success_message("K&auml;ytt&auml;j&auml; ".$edit_username." poistettu.");
            } else {
                echo utility_get_fail_message("K&auml;ytt&auml;j&auml;n poisto ep&auml;onnistui!");
            }
        } else {
            echo utility_get_fail_message("Et voi poistaa omaa tunnustasi!");
        }
    } else if (isset($_POST['new'])) {
        // Only admins are allowed to create new users
        echo utility_get_default_page_header("Luo uusi k&auml;ytt&auml;j&auml;", "Luo uusi k&auml;ytt&auml;j&auml;");
        $user = new User;
        echo $user->get_editor(TRUE, TRUE);
    } else {
        // All users get to edit their own user data, no matter the POST contents.
        echo utility_get_default_page_header("Muokkaa k&auml;ytt&auml;j&auml;n tietoja", "Muokkaa k&auml;ytt&auml;j&auml;n tietoja");

        $db = new Database;
        $db->open();
        $user = $db->get_user_info($edit_username);
        $db->close();

        if ($username == $edit_username) {
            // When admin edits their own data, make the editor appear as it would for normal users.
            echo $user->get_editor(FALSE, FALSE);
        } else {
            // Admins can edit other user's info
            echo $user->get_editor(FALSE, TRUE);
        }
    }
    
    // Return button
    echo "<hr>\n";
    echo "<form action=\"user_list.php\" method=\"GET\">\n";
    echo "<input type=\"submit\" value=\"Takaisin k&auml;ytt&auml;j&auml;listaan\" class=\"button\">\n";
    echo "</form>\n";
} else {
    $db = new Database;
    $db->open();
    $user = $db->get_user_info($edit_username);
    $db->close();
    
    // All users get to edit their own user data, no matter the POST contents.
    echo utility_get_default_page_header("Muokkaa k&auml;ytt&auml;j&auml;n tietoja", "Muokkaa k&auml;ytt&auml;j&auml;n tietoja");

	// Edit regular user's own info
	echo $user->get_editor(FALSE, $admin);

    // Return button
    echo "<hr>\n";
    echo "<form action=\"weekview.php\" method=\"GET\">\n";
    echo "<input type=\"submit\" value=\"Takaisin viikkon&auml;kym&auml;&auml;n\" class=\"button\">\n";
    echo "</form>\n";
}

echo utility_get_default_page_footer();

?>
