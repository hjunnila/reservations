<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.sourceforge.net>
  Copying is permitted under the terms of the BSD license. See COPYING.
*/

include "database.inc.php";
include "config.inc.php";
include "utility.inc.php";
include "login.inc.php";
include "token.inc.php";

$username = $_POST['username'];
$password = $_POST['password'];

// TODO: This is a bit dubious
$redirect_to = "weekview.php";
if (isset($_POST['redirect_to'])) {
    $redirect_to = $_POST['redirect_to'];
}

$db = new Database;
$db->open();

// Just to make it time consuming to run a dictionary attack against login.
sleep(2);

$user_status = $db->authenticate_user($username, $password);
if ($user_status == "user" || $user_status == "admin") {
    // Exactly one valid username & password was found. Create a token and store it into a cookie.
    $jwt = Token::create_token($username, ($user_status == "admin"));
    setcookie(COOKIE_TOKEN, $jwt, time() + COOKIE_TIME);

    // Track a successful login
    $db->track_user_activity($username, "login success", utility_get_client_headers(), utility_get_client_ip());

    echo utility_get_redirect_page_header($redirect_to, "Kirjaudutaan...", "Kirjaudutaan...");
} else {
    http_response_code(401);

    // Track a failed login
    $db->track_user_activity($username, "login failure", utility_get_client_headers(), utility_get_client_ip());

    // Error message
    echo utility_get_default_page_header("Kirjautuminen ep&auml;onnistui", "Kirjautuminen ep&auml;onnistui", null, false);
    echo login_get_failed();
}

// Close the database
$db->close();

// Default footer
echo utility_get_default_page_footer();

?>
