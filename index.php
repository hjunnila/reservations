<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include_once "utility.inc.php";
include_once "config.inc.php";
include_once "login.inc.php";

echo utility_get_default_page_header("Kirjautuminen", "Kirjautuminen", "onload=\"focus_username();\"", false);

// Display login form
echo login_get_form();

echo "<hr>";
echo "Varausj&auml;rjestelm&auml; vaatii, ett&auml; cookiet (ev&auml;steet) ovat k&auml;yt&ouml;ss&auml;.";

echo utility_get_default_page_footer();
?>
