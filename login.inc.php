<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

/**
 * Get a login form
 */
function login_get_form() {
    $login  = "<script>function focus_username() {";
    $login .= "document.login.username.focus(); }</script>";

    // POST the result to login.php
    $login .= "<form action=\"login.php\" method=\"POST\" name=\"login\">";

    $login .= "<table width=\"300\" rules=\"none\">";
    $login .= "<tr>";
    $login .= "<td class=\"entry_header\" align=\"right\">K&auml;ytt&auml;j&auml;tunnus</td>";
    $login .= "<td class=\"entry_data\"><input type=\"text\" name=\"username\"/></td>";
    $login .= "</tr>";

    $login .= "<tr>";
    $login .= "<td class=\"entry_header\" align=\"right\">Salasana</td>";
    $login .= "<td class=\"entry_data\"><input type=\"password\" name=\"password\"/></td>";
    $login .= "</tr>";

    $login .= "</table>";

    $login .= "<br>";
    $login .= "<input type=\"submit\" value=\"Kirjaudu\" class=\"button\">";

    $login .= "</form>";

    if (ANONYMOUS_BROWSE_ENABLED == true) {
        $login .= "<form action='weekview.php' method='GET'>";
        $login .= "<input type='submit' value='Selaa varauksia kirjautumatta' class='button'>";
        $login .= "</form>";
    }

    return $login;
}

/**
 * Get an error message for failed login
 *
 * @param message The message to display
 */
function login_get_failed($message = null)
{
    $msg  = "<font class=\"error_title\">";
    $msg .= "Kirjautuminen ep&auml;onnistui</font>\n";
    $msg .= "<br><br>";
    if ($message != null) {
        $msg .= "<font class=\"error_message\">$message</font>";
        $msg .= "<br><br>";
    }
    $msg .= "<form>\n";
    $msg .= "<input type=\"button\" value=\"Takaisin\" ";
    $msg .= "onClick=\"javascript: history.go(-1)\" class=\"button\">\n";
    $msg .= "</form>\n";

    return $msg;
}

?>
