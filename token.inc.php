<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

require "vendor/autoload.php";
use \Firebase\JWT\JWT;

class Token {
    static function current_token() {
        if (isset($_COOKIE[COOKIE_TOKEN])) {
            return $_COOKIE[COOKIE_TOKEN];
        } else {
            return null;
        }
    }

    static function create_token($username, $admin) {
        $key = file_get_contents(PEM_FILE);
        $expiration = time() + TOKEN_TIME;
        $token = array(
            "admin" => $admin,
            "uid" => $username,
            "exp" => $expiration,
            "criss" => CREDENTIALS_ISSUER
        );

        return JWT::encode($token, $key, 'RS256');
    }

    static function get_username($jwt) {
        $key = file_get_contents(PUB_FILE);
        try {
            $decoded = (array) JWT::decode($jwt, $key, array('RS256'));
        } catch (Exception $e) {
            return FALSE;
        }

        if ($decoded) {
            return $decoded["uid"];
        } else {
            return null;
        }
    }

    static function get_admin($jwt) {
        $key = file_get_contents(PUB_FILE);
        try {
            $decoded = (array) JWT::decode($jwt, $key, array('RS256'));
        } catch (Exception $e) {
            return FALSE;
        }

        if ($decoded) {
            if ($decoded["admin"] == TRUE) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function is_authorized($jwt) {
        $key = file_get_contents(PUB_FILE);
        try {
            $decoded = JWT::decode($jwt, $key, array('RS256'));
        } catch (Exception $e) {
            return FALSE;
        }

        if ($decoded->criss != CREDENTIALS_ISSUER) {
            // Wrong issuer
            return FALSE;
        }

        if (time() > $decoded->exp) {
            // Token has expired
            return FALSE;
        }

        return TRUE;
    }
}

?>
