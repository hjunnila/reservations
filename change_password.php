<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "token.inc.php";
include "utility.inc.php";
include "database.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not (properly) logged in, redirect to index.php
	echo utility_get_redirect_page_header("index.php");
	echo utility_get_default_page_footer();
	die();
}

// Check the current user's admin status
$admin_status = Token::get_admin($jwt);

// Current user
$username = Token::get_username($jwt);

// POST parameters
$edit_username = $_POST['username'];
$oldpassword = $_POST['oldpassword'];
$newpassword1 = $_POST['newpassword1'];
$newpassword2 = $_POST['newpassword2'];

echo utility_get_default_page_header("Salasanan vaihto", "Salasanan vaihto");

// Admins can change anyone's password, while users can change only their own
if ($admin_status || $edit_username == $username) {
    if ($newpassword1 != $newpassword2) {
        // New passwords do not match each other
        echo utility_get_fail_message("Salasanan vaihto ep&auml;onnistui", "Uudet salasanat eiv&auml;t t&auml;sm&auml;&auml; kesken&auml;&auml;n!");
        echo utility_get_default_page_footer();
        die();
    } else if (strlen($oldpassword) == 0 && $admin_status == FALSE) {
        // Only admins can proceed without knowing the current password
		echo utility_get_fail_message("Salasanan vaihto ep&auml;onnistui", "Nykyinen salasana ei t&auml;sm&auml;&auml;!");
        echo utility_get_default_page_footer();
        die();
    } else if (strlen($newpassword1) < PASSWORD_MIN_LENGTH) {
        // Password is too short
		echo utility_get_fail_message("Salasanan vaihto ep&auml;onnistui", "Uusi salasana on liian lyhyt!");
        echo utility_get_default_page_footer();
        die();
    }

    $db = new Database;
    $db->open();
    if (!$admin_status || $edit_username == $username) {
        // Admin can change others' passwords without knowing them, but admin must know their
        // own password to change it.
        $result = $db->authenticate_user($edit_username, $oldpassword);
        if ($result != "user" && $result != "admin") {
            $db->close();
            echo utility_get_fail_message("Salasanan vaihto ep&auml;onnistui", "Nykyinen salasana ei t&auml;sm&auml;&auml;!");
            echo utility_get_default_page_footer();
            die();
        }
    }

    // Set new password
    $result = $db->set_user_password($edit_username, $newpassword1);
    $db->close();

    if ($result) {
        echo utility_get_success_message("Salasanan vaihto onnistui", "Salasana vaihdettu");
    } else {
		echo utility_get_fail_message("Salasanan vaihto ep&auml;onnistui", "Salasanan vaihto ep&auml;onnistui!");
    }
} else {
    // A non-admin attempting to change another user's password is not permitted
    echo utility_get_fail_message("Salasanan vaihto ep&auml;onnistui", "Et voi muuttaa k&auml;ytt&auml;j&auml;n salasanaa!");
}

echo utility_get_default_page_footer();

?>
