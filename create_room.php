<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "database.inc.php";
include "token.inc.php";
include "utility.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
    // We are not (properly) logged in, redirect to index.php
    echo utility_get_redirect_page_header("index.php");
    echo utility_get_default_page_footer();
    die();
} else if (!Token::get_admin($jwt)) {
    // Only admins are allowed to this file, redirect others to weekview.
    echo utility_get_redirect_page_header("weekview.php");
    echo utility_get_default_page_footer();
    die();
}

// Get new room info
$room = new Room;
$room->name = $_POST['room_name'];

if ($room->name == null) {
    // Room name is missing
    echo utility_get_default_page_header("Huoneen luonti ep&auml;onnistui", "Huoneen luonti ep&auml;onnistui");
    echo utility_get_fail_message("Huoneella ei ole nime&auml;!");
    die();
} else {
    $db = new Database;
    $db->open();

    $result = $db->create_room($room);
    if ($result) {
        echo utility_get_default_page_header("Huoneen luonti onnistui", "Huoneen luonti onnistui");
        echo utility_get_success_message("Huone <b>".$room->name."</b> luotu.");
    } else {
        echo utility_get_default_page_header("Huoneen luonti ep&auml;onnistui",  "Huoneen luonti ep&auml;onnistui");
        echo utility_get_fail_message("Huoneen sy&ouml;tt&ouml; tietokantaan ep&auml;onnistui.");
    }
}

echo utility_get_default_page_footer();

?>
