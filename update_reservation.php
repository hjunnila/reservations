<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "utility.inc.php";
include "token.inc.php";
include "database.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not (properly) logged in, redirect to index.php
	echo utility_get_redirect_page_header("index.php");
	echo utility_get_default_page_footer();
	die();
}

$username = Token::get_username($jwt);
$admin_status = Token::get_admin($jwt);

// Get the reservation id first from POST parameters
$res_id = $_POST['reservation_id'];
$type = $_POST['type'];

// Fetch existing reservation details
$db = new Database;
$db->open();
$reservation = $db->get_reservation_details_by_type($res_id, $type);
if (!$reservation) {
    // Something is wrong
    echo utility_get_default_page_header("Muokkaa varausta", "Muokkaa varausta");
    $message = "Varausta ei l&ouml;ytynyt!";
    echo utility_get_fail_message("P&auml;ivitys ep&auml;onnistui!", $message);
    die();
}

// Insert new reservation details to the existing reservation object
$reservation->start = $_POST['start'];
$reservation->end = $_POST['end'];
$reservation->purpose = trim($_POST['purpose']);
$reservation->description = $_POST['description'];

echo utility_get_default_page_header("Muokkaa varausta", "Muokkaa varausta");

// Check that the reservation is current user's own or current user is admin
if ($reservation->reservee->username != $username && !$admin_status) {
    $message = "Et voi muokata muiden tekemi&auml; varauksia!<br>\n";
    echo utility_get_fail_message("P&auml;ivitys ep&auml;onnistui!", $message);
    echo utility_get_default_page_footer();
    die();
}

// Check that the hours are correct. 
if (!$reservation->check_times()) {
    $message = "Tarkista alku- ja loppuaika.<br/>\n";
    $message .= "Tilavarauksia voi tehd&auml; ajalle ";
    $message .= RESERVATION_FIRST_HOUR.":00 - ".RESERVATION_LAST_HOUR.":00.\n";
    $message .= "<br>Vuorokauden ylitt&auml;v&auml;t varaukset tulee tehd&auml; kahdessa osassa.<br>\n";
    echo utility_get_fail_message("P&auml;ivitys ep&auml;onnistui!", $message);
    echo utility_get_default_page_footer();
    die();
}

// Check that there is a purpose for the reservation
if (strlen($reservation->purpose) == 0) {
    $message = "Et sy&ouml;tt&auml;nyt varauksellesi k&auml;ytt&ouml;tarkoitusta.<br>\n";
    echo utility_get_fail_message("P&auml;ivitys ep&auml;onnistui!", $message);
    echo utility_get_default_page_footer();
    die();
}

// Start a transaction
$db->begin();

$overlaps = 0;
$weekly_overlaps = 0;

if ($type == RESERVATION_TYPE_SINGLE) {
    // Check that the time doesn't overlap with other reservations
    $overlaps = $db->get_overlapping_reservations($reservation);

    // Check that the time doesn't overlap with weekly reservations
    $weekly_overlaps = $db->get_overlapping_weekly($reservation);
}

if ($overlaps < 0 || $weekly_overlaps < 0) {
	// Unable to query overlaps
	$db->rollback();
	$message = "Virhe tietojen v&auml;lityksess&auml;! Ota yhteys yll&auml;pitoon.";
	echo utility_get_fail_message("P&auml;ivitys ep&auml;onnistui!", $message);
} else if ($overlaps == 0 && $weekly_overlaps == 0) {
	if (!$db->update_reservation($reservation)) {
	    // Unable to update
		$db->rollback();
		$message  = "Virhe tietojen v&auml;lityksess&auml;! Ota yhteys yll&auml;pitoon.";
		echo utility_get_fail_message("P&auml;ivitys ep&auml;onnistui!", $message);
	} else {
	    // Updated successfully
		$db->commit();
		echo utility_get_success_message("P&auml;ivitys onnistui!");
	}
} else {
    // There are overlapping reservations
	$db->rollback();
	$message  = "Varauksesi menee p&auml;&auml;llek&auml;in toisen varauksen kanssa.<br>";
	echo utility_get_fail_message("P&auml;ivitys ep&auml;onnistui!", $message);
}

$db->close();

echo utility_get_default_page_footer();

?>
