<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "token.inc.php";
include "utility.inc.php";
include "database.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not (properly) logged in, redirect to index.php
	echo utility_get_redirect_page_header("index.php");
	echo utility_get_default_page_footer();
	die();
} else if (!Token::get_admin($jwt)) {
	// The user is not an admin, redirect to weekview
	echo utility_get_redirect_page_header("weekview.php");
	echo utility_get_default_page_footer();
	die();
}

// Get the user list from database
$db = new Database;
$db->open();
$users = $db->get_user_list();
$db->close();

echo utility_get_default_page_header("K&auml;ytt&auml;jien hallinta", "K&auml;ytt&auml;jien hallinta");

echo User::get_list_view($users);

echo utility_get_default_page_footer();

?>
