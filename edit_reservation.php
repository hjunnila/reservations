<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "database.inc.php";
include "utility.inc.php";
include "token.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not (properly) logged in, redirect to index.php
	echo utility_get_redirect_page_header("index.php");
	echo utility_get_default_page_footer();
	die();
}

$username = Token::get_username($jwt);
$admin = Token::get_admin($jwt);

// Get the reservation
$res_id = $_GET['reservation_id'];
$type = $_GET['type'];

// Get reservation & current user data
$db = new Database;
$db->open();
$reservation = $db->get_reservation_details_by_type($res_id, $type);
$current_user = $db->get_user_info($username);
$db->close();

if (!$reservation) {
    // Reservation does not exist
	echo utility_get_redirect_page_header("weekview.php");
    echo utility_get_default_page_footer();
    die();
} else if ($reservation->reservee->username != $username && !$admin) {
    // No authority to edit the reservation
	echo utility_get_redirect_page_header("weekview.php");
    echo utility_get_default_page_footer();
    die();
} else {
    // Insert default page header
    echo utility_get_default_page_header("Muokkaa varausta", "Muokkaa varausta");

    // Insert the reservation editor
    echo $reservation->get_editor(RESERVATION_METHOD_UPDATE, $current_user);

    // Insert default page footer
    echo utility_get_default_page_footer();
}

?>
