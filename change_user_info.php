<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "token.inc.php";
include "database.inc.php";
include "utility.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not (properly) logged in, redirect to index.php
	echo utility_get_redirect_page_header("index.php");
	echo utility_get_default_page_footer();
	die();
}

// Check the current user's admin status
$admin_status = Token::get_admin($jwt);

// Get the username who is being edited
$edit_username = $_POST['username'];

// Admins can change anyone's info, while regular users only their own info
if ($admin_status || $edit_username == Token::get_username($jwt)) {
    $realname = $_POST['realname'];
    $telephone = $_POST['telephone'];
    $email = $_POST['email'];
    $admin = FALSE;

    // Only admins can promote/demote others' admin status.
    if ($admin_status && isset($_POST['admin'])) {
    	// Convert the checkbox result to be compatible.
        if ($_POST['admin'] == "on") {
            $admin = TRUE;
        }
    } else if ($admin_status && $edit_username == Token::get_username($jwt)) {
        // Admin is editing his own data. Make sure admins don't demote themselves (due to missing data).
        $admin = TRUE;
    }

	// Do the update
	$user = new User;
	$user->username = $edit_username;
	$user->realname = $realname;
	$user->telephone = $telephone;
	$user->email = $email;
	$user->admin = $admin;

	$db = new Database;
	$db->open();
	$result = $db->change_user_info($user);
	$db->close();

    echo utility_get_default_page_header("K&auml;ytt&auml;j&auml;tietojen p&auml;ivitys", "K&auml;ytt&auml;j&auml;tietojen p&auml;ivitys");

    if ($result) {
        echo utility_get_success_message("K&auml;ytt&auml;j&auml;tietojen p&auml;ivitys onnistui.");
    } else {
        echo utility_get_fail_message("K&auml;ytt&auml;j&auml;tietojen p&auml;ivitys ep&auml;onnistui!");
    }

    echo utility_get_default_page_footer();
} else {
    // A non-admin attempting to change another user's info is not permitted
    echo utility_get_redirect_page_header("index.php");
    echo utility_get_default_page_footer();
    die();
}

?>
