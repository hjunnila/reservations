<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "token.inc.php";
include "database.inc.php";
include "utility.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not (properly) logged in, redirect to index.php
	echo utility_get_redirect_page_header("index.php");
	echo utility_get_default_page_footer();
	die();
} else if (!Token::get_admin($jwt)) {
    // Only admins are allowed to this file, redirect others to weekview.
    echo utility_get_redirect_page_header("weekview.php");
    echo utility_get_default_page_footer();
    die();
}

$room = new Room;
$room->id = $_POST['room_id'];
$room->name = $_POST['room_name'];

$db = new Database;
$db->open();
$result = $db->change_room_info($room);
$db->close();

echo utility_get_default_page_header("Huoneen tietojen p&auml;ivitys", "Huoneen tietojen p&auml;ivitys");

if ($result) {
    echo utility_get_success_message("Huoneen tietojen p&auml;ivitys onnistui.");
} else {
    echo utility_get_fail_message("Huoneen tietojen p&auml;ivitys ep&auml;onnistui!");
}

echo utility_get_default_page_footer();

?>
