<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "utility.inc.php";
include "database.inc.php";
include "token.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
    // We are not (properly) logged in, redirect to index.php
    echo utility_get_redirect_page_header("index.php");
    echo utility_get_default_page_footer();
    die();
}

// Get info about current user
$username = Token::get_username($jwt);
$admin = Token::get_admin($jwt);

// Create a new reservation object with the given data
$reservation = new Reservation;
$reservation->type = RESERVATION_TYPE_SINGLE;
$reservation->room_id = $_POST['room'];
$reservation->date = $_POST['date'];
$reservation->weekday = $_POST['weekday'];
$reservation->start = $_POST['start'];
$reservation->end = $_POST['end'];
$reservation->purpose = trim($_POST['purpose']);
$reservation->description = $_POST['description'];

if ($admin && $_POST['recurrence'] == RESERVATION_TYPE_WEEKLY) {
    // Only admins can change reservation type
    $reservation->type = RESERVATION_TYPE_WEEKLY;
}

// Get more info about the current user and put it into the reservee field
$db = new Database;
$db->open();
$reservation->reservee = $db->get_user_info($username);

echo utility_get_default_page_header("Uusi tilavaraus");

// Check that the hours are correct. 
if (!$reservation->check_times()) {
	$message = "Tarkista alku- ja loppuaika.<br/>\n";
	$message .= "Tilavarauksia voi tehd&auml; ajalle ".RESERVATION_FIRST_HOUR.":00 - ".RESERVATION_LAST_HOUR.":00.<br/>";
	$message .= "Vuorokauden ylitt&auml;v&auml;t varaukset tulee tehd&auml; kahdessa osassa.<br/>\n";
	echo utility_get_fail_message("Varaus ep&auml;onnistui!", $message);
	die();
}

// Check that there is a purpose for the reservation
if (strlen($reservation->purpose) == 0) {
	$message = "Et sy&ouml;tt&auml;nyt varauksellesi k&auml;ytt&ouml;tarkoitusta.<br/>\n";
	echo utility_get_fail_message("Varaus ep&auml;onnistui!", $message);
	die();
}

$db->begin();

$overlaps = 0;
$weekly_overlaps = 0;

if ($reservation->type == RESERVATION_TYPE_SINGLE) {
    // Check that the time doesn't overlap with other reservations
    $overlaps = $db->get_overlapping_reservations($reservation);

    // Check that the time doesn't overlap with weekly reservations
    $weekly_overlaps = $db->get_overlapping_weekly($reservation);
}

if ($overlaps < 0 || $weekly_overlaps < 0) {
    // Unable to query overlaps
    $db->rollback();
    $message = "Virhe tietojen v&auml;lityksess&auml;! Ota yhteys yll&auml;pitoon.";
    echo utility_get_fail_message("Varaus ep&auml;onnistui!", $message);
} else if ($overlaps == 0 && $weekly_overlaps == 0) {
    if (!$db->create_reservation($reservation)) {
        // Unable to create
        $db->rollback();
        $message  = "Varausta ei voida tehd&auml;! Ota yhteys yll&auml;pitoon.";
        echo utility_get_fail_message("Varaus ep&auml;onnistui!", $message);
    } else {
        // Updated successfully
        $db->commit();
        echo utility_get_success_message("Varaus onnistui!");
    }
} else {
    // There are overlapping reservations
	$db->rollback();
	$message  = "Varauksesi menee p&auml;&auml;llek&auml;in toisen varauksen kanssa.<br>";
    echo utility_get_fail_message("Varaus ep&auml;onnistui!", $message);
}

$db->close();

echo utility_get_default_page_footer();

?>
