<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "utility.inc.php";
include "logout.inc.php";
include "token.inc.php";
include "database.inc.php";

$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
   // We are not (properly) logged in. No reason to logout. Redirect to index.php
   echo utility_get_redirect_page_header("index.php");
   echo utility_get_default_page_footer();
   die();
}

$db = new Database;
$db->open();
$user = $db->get_user_info(Token::get_username($jwt));
$db->track_user_activity($user->username, "logout", utility_get_client_headers(), utility_get_client_ip());
$db->close();

logout();
echo get_logout_message();

?>
