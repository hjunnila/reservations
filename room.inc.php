<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

class Room {
    var $id;
    var $name;

    /**
     * Get a room editor form for the instance.
     *
     * @param new_room If TRUE, the editor is used to create a new room
     */
    function get_editor($new_room = FALSE) {
        if ($new_room) {
            $view = "<form action=\"create_room.php\" method=\"post\" name=\"roominfo\">\n";
        } else {
            $view = "<form action=\"change_room_info.php\" method=\"post\" name=\"roominfo\">\n";
        }

        $view .= "<table width=\"300\" rules=\"none\" cellspacing=\"0\" cellpadding=\"0\">\n";

        // Room ID
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">ID</td>\n";
        $view .= "<td class=\"entry_data\">\n";
        $view .= "<input type=\"hidden\" name=\"room_id\" value=\"".$this->id."\">\n";
        $view .= "<input type=\"text\" name=\"room_id\" value=\"".$this->id."\" disabled>\n";
        $view .= "</td>\n";
        $view .= "</tr>\n";

        // Room name
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_header\">Nimi</td>\n";
        $view .= "<td class=\"entry_data\">\n";
        $view .= "<input type=\"text\" name=\"room_name\" value=\"".$this->name."\">\n";
        $view .= "</td>\n";
        $view .= "</tr>\n";

        // Update/Create button
        $view .= "<tr>\n";
        $view .= "<td>&nbsp;</td>\n";
        $view .= "<td class=\"entry_data\">\n";

        if ($new_room) {
            $view .= "<input type=\"submit\" value=\"Luo\" class=\"button\">\n";
        } else {
            $view .= "<input type=\"submit\" value=\"P&auml;ivit&auml;\" class=\"button\">\n";
        }

        $view .= "</td>\n";
        $view .= "</tr>\n";

        // Close the table & form
        $view .= "</table>\n";
        $view .= "</form>\n";
        
        return $view;
    }

    /**
     * Return a selection box for available rooms.
     *
     * @param rooms An array of Room objects
     * @param preselected The ID or name of the room to preselect in the box
     * @return A string containing a <select> entry to be used in a form
     */
    static function get_selection_box($rooms, $preselected = null) {
        $combo = "<select name='room' onChange='document.RoomSelectionForm.submit()'>\n";

        foreach ($rooms as $room) {
            if ($preselected == $room->id || $preselected == $room->name) {
                $combo .= "<option value=\"".$room->id."\" selected>".$room->name."</option>\n";
            } else {
                $combo .= "<option value=\"".$room->id."\">".$room->name."</option>\n";
            }
        }

        $combo .= "</select>\n";
        
        return $combo;
    }
    
    /**
     * Get the room list view
     *
     * @param rooms An array of Room objects
     * @return A string containing the room list form
     */
    static function get_list_view($rooms) {
        $view = "<script type=\"text/javascript\">";
        $view .= "function confirm_room_removal() { var sel = document.RoomControlForm.room_id; var option = sel.options[sel.selectedIndex]; return confirm('Poistetaanko huone: ' + option.text + '?'); }";
        $view .= "</script>\n";
        $view .= "<table width=\"300\">\n";
        $view .= "<form action=\"edit_room.php\" method=\"POST\" name=\"RoomControlForm\">\n";
        $view .= "<tr>\n";
        $view .= "<td class=\"entry_data\">\n";
        $view .= Room::get_select_structure($rooms);
        $view .= "</td>\n";
        $view .= "<td valign=\"top\">\n";
        $view .= "<input type=\"submit\" name=\"edit\" value=\"Muokkaa\" class=\"button\">\n";
        $view .= "<br>\n";
        $view .= "<input type=\"submit\" name=\"remove\" value=\"Poista\" class=\"button\" onClick=\"return confirm_room_removal()\">\n";
        $view .= "<br>\n";
        $view .= "<input type=\"submit\" name=\"new\" value=\"Uusi\" class=\"button\">\n";
        $view .= "</td>\n";
        $view .= "</tr>\n";
        $view .= "</form>\n";
        $view .= "</table>\n";
        $view .= "<hr>\n";
        $view .= "<form action=\"weekview.php\" method=\"POST\">\n";
        $view .= "<input type=\"submit\" value=\"Takaisin viikkon&auml;kym&auml;&auml;n\" class=\"button\">\n";
        $view .= "</form>\n";

        return $view;
    }

    /**
     * Insert the given array of rooms into a <select> structure and return the resulting HTML
     *
     * @param rooms An array of Room objects to be inserted into a form as options
     * @return A <select> structure containing the given rooms
     */
    static function get_select_structure($rooms) {
        $list = "<select size=\"20\" name=\"room_id\">\n";
        foreach ($rooms as $room) {
            $list .= "<option value=\"".$room->id."\">";
            $list .= "[".$room->id."] ".$room->name."</option>\n";
        }

        $list .= "</select>\n";

        return $list;
    }
}

?>
