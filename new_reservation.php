<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "utility.inc.php";
include "token.inc.php";
include "database.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not (properly) logged in, redirect to index.php
	echo utility_get_redirect_page_header("index.php");
	echo utility_get_default_page_footer();
	die();
}

$username = Token::get_username($jwt);
$admin = Token::get_admin($jwt);

$type = $_GET['type'];
$unixtime = $_GET['unixtime'];
$room_id = $_GET['room'];
$db = new Database;
$db->open();
$room = $db->get_room($room_id);
$user = $db->get_user_info($username);
$db->close();

// Insert default page header
echo utility_get_default_page_header("Uusi tilavaraus", "Uusi tilavaraus", "onload=\"focus_purpose();\"");

if (!$room || !$user) {
    // Unable to get room/user info
    echo utility_get_fail_message("Tietoja ei voida hakea.");
} else {
    // Fill up a new reservation entry
    $reservation = new Reservation;
    $reservation->type = $type;
    $reservation->room_id = $room->id;
    $reservation->room_name = $room->name;
    $reservation->date = date("Y-m-d", $unixtime);
    $reservation->reservee = $user;
    $reservation->start = date("H", $unixtime);
    $reservation->end = $reservation->start + 1;

    // Weekday is used only if type == WEEKLY but it doesn't hurt if it's there in any case
    $date_array = getdate($unixtime);
    $reservation->weekday = $date_array["wday"];

    // Insert the reservation editor and POST it to the reservation insertion script
    echo $reservation->get_editor(RESERVATION_METHOD_CREATE, $user);
}

// Insert default page footer
echo utility_get_default_page_footer();

?>
