#!/bin/bash
#
# Create an RSA256 key pair to be used for the JWT signing in the room reservation system.
# After you have run this script you should move the .pub and .key files somewhere so that
# they are accessible for your web server & PHP instance but not accessible from outside of
# your box (for example with a simple 'wget http://your.server.com/reservations/jwt256.key').
#

ssh-keygen -t rsa -b 2048 -f jwtRS256.key
# Don't add passphrase
openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub
cat jwtRS256.key
cat jwtRS256.key.pub
