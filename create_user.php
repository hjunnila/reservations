<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "database.inc.php";
include "token.inc.php";
include "utility.inc.php";

// Check, whether we are logged in
$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
    // We are not (properly) logged in, redirect to index.php
    echo utility_get_redirect_page_header("index.php");
    echo utility_get_default_page_footer();
    die();
}

$admin_status = Token::get_admin($jwt);
if (!$admin_status) {
    // Only admins are allowed to this file, redirect others to weekview.
    echo utility_get_redirect_page_header("weekview.php");
    echo utility_get_default_page_footer();
    die();
}

// Get new user info
$user = new User;
$user->username = $_POST['username'];
$user->realname = $_POST['realname'];
$user->telephone = $_POST['telephone'];
$user->email = $_POST['email'];
$user->admin = isset($_POST['admin']) ? $_POST['admin'] : FALSE;
$new_password1 = $_POST['newpassword1'];
$new_password2 = $_POST['newpassword2'];

if ($user->username == null) {
    // Username is missing
    echo utility_get_default_page_header("K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui",
                                         "K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui");
    echo utility_get_fail_message("K&auml;ytt&auml;j&auml;tunnus puuttuu!");
    die();
} else if ($user->realname == null) {
    // Real name is missing
    echo utility_get_default_page_header("K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui", 
                                         "K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui");
    echo utility_get_fail_message("K&auml;ytt&auml;j&auml;n nimi puuttuu!");
    die();
} else if ($user->telephone == null) {
    // Telephone number is missing
    echo utility_get_default_page_header("K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui", 
                                         "K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui");
    echo utility_get_fail_message("Puhelinnumero puuttuu!");
    die();
} else if ($user->email == null) {
    // Email address is missing
    echo utility_get_default_page_header("K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui", 
                                         "K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui");
    echo utility_get_fail_message("S&auml;hk&ouml;posti puuttuu!");
    die();
} else if ($new_password1 == null || $new_password1 != $new_password2) {
    // Password is empty or they do not match
    echo utility_get_default_page_header("K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui", 
                                         "K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui");
    echo utility_get_fail_message("Salasanat eiv&auml;t t&auml;sm&auml;&auml;!");
    die();
} else {
    $db = new Database;
    $db->open();

    // Check that a user by the same username doesn't already exist
	$existing_user = $db->get_user_info($user->username);
	if ($existing_user == (object) null) {
        $result = $db->create_user($user, $new_password1);
		if ($result) {
            echo utility_get_default_page_header("K&auml;ytt&auml;j&auml;n luonti onnistui", 
                                                 "K&auml;ytt&auml;j&auml;n luonti onnistui");
            echo utility_get_success_message("K&auml;ytt&auml;j&auml;tunnus <b>".$user->username."</b> luotu.");
        } else {
            echo utility_get_default_page_header("K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui", 
                                                 "K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui");
            echo utility_get_fail_message("Tunnuksen sy&ouml;tt&ouml; tietokantaan ep&auml;onnistui.");
        }
    } else {
        // Username exists
        echo utility_get_default_page_header("K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui", 
                                             "K&auml;ytt&auml;j&auml;n luonti ep&auml;onnistui");
        echo utility_get_fail_message("K&auml;ytt&auml;j&auml;tunnus <b>".$existing_user->username."</b> (".$existing_user->realname.") on jo olemassa!");
	}
}

echo utility_get_default_page_footer();

?>
