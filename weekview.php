<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "token.inc.php";
include "config.inc.php";
include "utility.inc.php";
include "database.inc.php";
include "weekview.inc.php";

if (ANONYMOUS_BROWSE_ENABLED == false) {
    // Check, whether we are logged in
    $jwt = Token::current_token();
    if (!$jwt || !Token::is_authorized($jwt)) {
           // We are not (properly) logged in, redirect to index.php
           echo utility_get_redirect_page_header("index.php");
           echo utility_get_default_page_footer();
           die();
    }
}

// Get the room under inspection
$room_id = 1;
if (isset($_GET['room'])) {
    // Room was given in GET parameter, save it to a cookie
    $room_id = $_GET['room'];
    setcookie(COOKIE_ROOM, $room_id, time() + COOKIE_TIME);
} else {
	// No room id given on URL, try to get the current room from a cookie
	if (isset($_COOKIE[COOKIE_ROOM])) {
		$room_id = $_COOKIE[COOKIE_ROOM];
	}
}

// Get the date under inspection
$unixtime = time();
if (isset($_GET['date'])) {
	// Date was given in GET parameter, save it to a cookie
    $unixtime = $_GET['date'];
	setcookie(COOKIE_UNIXTIME, $unixtime, time() + COOKIE_TIME);
} else {
	// No date given, try to get the date from a cookie
    if (isset($_COOKIE[COOKIE_UNIXTIME])) {
    	$unixtime = $_COOKIE[COOKIE_UNIXTIME];
    }
}

// Always start the week view from monday
$unixtime = utility_get_monday_of_week($unixtime);

// Get some stuff from database
$db = new Database;
$db->open();
$reservations = $db->get_reservations($room_id, $unixtime);
$weekly_reservations = $db->get_weekly_reservations($room_id);
$rooms = $db->get_rooms();
$room = $db->get_room($room_id);

$jwt = Token::current_token();
if (!$jwt || !Token::is_authorized($jwt)) {
	// We are not logged in, readonly mode activated
    $user = (object) null;
} else {
    // User is logged in, get their info
    $user = $db->get_user_info(Token::get_username($jwt));
}

$db->close();

// Echo the default page header with room name
echo utility_get_default_page_header($room->name, $room->name);

// Render the actual week view using reservation data, current time and user info
echo weekview_get_view($reservations, $weekly_reservations, $unixtime, $user, $room_id);

// Insert the control bar
echo weekview_get_control_bar($rooms, $room_id, $unixtime, $user);

// Insert the default page footer
echo utility_get_default_page_footer();

?>
