<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.sourceforge.net>
  Copying is permitted under the terms of the BSD license. See COPYING.
*/

/**
 * Logout. Basically just sets an expired token cookie, which is then deleted by the browser.
 */
function logout() {
    setcookie(COOKIE_TOKEN, null, time() - COOKIE_TIME);
}

/**
 * Get a logout message, which eventually takes the user back to login screen.
 */
function get_logout_message() {
    $msg =  "<html>\n";
    $msg .= "<head>\n";
    $msg .= "<title>Uloskirjautuminen</title>\n";
    $msg .= "<link href=\"reservation.css\" rel=\"stylesheet\" ";
    $msg .= "type=\"text/css\"/>\n";
    $msg .= "<meta http-equiv=\"Refresh\" content=\"1;URL=index.php\">";
    $msg .= "</head>\n";
    $msg .= "</html>";

    return $msg;
}

?>
