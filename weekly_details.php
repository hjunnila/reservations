<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

include "config.inc.php";
include "utility.inc.php";
include "token.inc.php";
include "database.inc.php";

$username = null;
$jwt = Token::current_token();

if (ANONYMOUS_BROWSE_ENABLED == false) {
    // Check, whether we are logged in
    if (!$jwt || !Token::is_authorized($jwt)) {
       // We are not (properly) logged in, redirect to index.php
       echo utility_get_redirect_page_header("index.php");
       echo utility_get_default_page_footer();
       die();
    }
}

$weekly_id = $_GET['weekly_id'];

$db = new Database;
$db->open();
$weekly = $db->get_weekly_details($weekly_id);

if (!$jwt || !Token::is_authorized($jwt)) {
    $current_user = null;
} else {
    $username = Token::get_username($jwt);
    $current_user = $db->get_user_info($username);
}
$db->close();

// Insert default page header
echo utility_get_default_page_header("Viikoittaisen varauksen tiedot", "Viikoittaisen varauksen tiedot");

if (!$weekly) {
    echo utility_get_fail_message("Varauksen tietoja ei l&ouml;ydy!");
} else {
    echo $weekly->get_details_view($current_user);
}

echo utility_get_default_page_footer();

?>
