<?php
/*
  Copyright 2017 Heikki Junnila <hjunnila@users.noreply.github.com>
  Copying is permitted under the terms of the BSD license, see COPYING.
*/

/**
 * Get an information link for a single reserved slot
 *
 * @param res_id The unique ID of the reservation
 * @param purpose The purpose of the reservation
 * @param own true if the reservation is the user's own, otherwise false
 */
function weekview_get_reservation_info($res_id, $purpose, $own) {
    $len = strlen($purpose);
    $short_purpose = substr($purpose, 0, 13);
    $short_purpose = str_replace(" ", "&nbsp;", $short_purpose);
    if ($len > 13) {
		$short_purpose .= "...";
    }

    if ($own == true) {
        $info = "<td class=\"weekview_own_slot\">";
        $info .= "<a href=\"reservation_details.php?";
        $info .= "reservation_id=$res_id\" title=\"Oma varaus: $purpose\">";
        $info .= "<font class=\"weekview_own_slot\">$short_purpose</font></a>";
        $info .= "</td>\n";
    } else {
        $info = "<td class=\"weekview_other_slot\">";
        $info .= "<a href=\"reservation_details.php?";
        $info .= "reservation_id=$res_id\" title=\"Varattu: $purpose\">";
        $info .= "<font class=\"weekview_other_slot\">$short_purpose</font></a>";
        $info .= "</td>\n";
    }

    return $info;
}

/**
 * Get an information link for a single weekly fixed slot
 *
 * @param weekly_id The unique ID of the reservation
 * @param purpose The purpose of the reservation
 */
function weekview_get_weekly_info($weekly_id, $purpose) {
    $len = strlen($purpose);
    $short_purpose = substr($purpose, 0, 13);
    $short_purpose = str_replace(" ", "&nbsp;", $short_purpose);
    if ($len > 13) {
		$short_purpose .= "...";
    }

    $info  = "<td align=\"center\" class=\"weekview_weekly_slot\">";
    $info .= "<a href=\"weekly_details.php?weekly_id=$weekly_id\" title=\"Pysyv&auml;sti varattu: $purpose\">";
    $info .= "<font class=\"weekview_weekly_slot\">$short_purpose</font></a>";
    $info .= "</td>\n";

    return $info;
}

/**
 * Get an information link for a single free slot
 *
 * @param unixtime The actual time of the free slot (date & time)
 * @param room The room where this free slot is
 * @param user The current user
 */
function weekview_get_free_slot_info($unixtime, $room, $user) {
    $current_array = getdate();
    $slot_array = getdate($unixtime);

    if ($current_array["year"] >= $slot_array["year"] && $current_array["yday"] > $slot_array["yday"]) {
        /* Block this so users can't create reservations in the past */
        if ($user != null) {
            $info = "<td class=\"weekview_past_slot\">Vapaa</td>\n";
        } else {
            $info = "<td class=\"weekview_free_slot\">---</td>";
        }
    } else {
        // Normal free slot
        $info = "<td class=\"weekview_free_slot\">";
        if ($user != null) {
            $info .= "<a href=\"new_reservation.php?";
            $info .= "unixtime=$unixtime&room=$room&type=single\" ";
            $info .= "title=\"Tee varaus ";
            $info .= utility_get_finnish_weekday($slot_array["wday"], true);
            $info .= "lle, kello ".$slot_array["hours"].":00 eteenp&auml;in\">";
            $info .= "<font class=\"weekview_free_slot\">Varaa</font>";
            $info .= "</a>";
        } else {
            $info .= "<font class=\"weekview_free_slot\">---</font>";
        }
        $info .= "</td>\n";
    }

    return $info;
}

/**
 * Get a complete table row for a full week, starting from the given date
 *
 * @param unixtime The starting date
 * @param room The id of the room id to use
 * @param user Current user
 */
function weekview_get_weekdays($unixtime, $room) {
    // Get today so we can mark it with bold text
    $today_array = getdate(time());

    // Insert dates
    $weekdays = "<tr>\n";
    $weekdays .= "<td>&nbsp;</td>\n";

    $i = 0;
    while ($i < 7) {
        $date = strtotime("+$i days", $unixtime);
        $date_array = getdate($date);

        $weekdays .= "<td class=\"weekview_dates\">";

        // Mark today
        if ($date_array["year"] == $today_array["year"] &&
            $date_array["mon"] == $today_array["mon"] &&
            $date_array["mday"] == $today_array["mday"]) {
            $weekdays .= "<b><i>";
        }

        // Insert the date in finnish format
        $weekdays .= date("d.m.", $date);

        // Close today emphasis
        if ($date_array["year"] == $today_array["year"] &&
            $date_array["mon"] == $today_array["mon"] &&
            $date_array["mday"] == $today_array["mday"]) {
            $weekdays .= "</i></b>";
        }

        $weekdays .= "</td>\n";
        $i++;
    }
    $weekdays .= "</tr>\n";

    // Insert day names
    $weekdays .= "<tr>\n";
    $weekdays .= "<td class=\"weekview_weekdays\">Kellonaika</td>\n";

    $date = $unixtime;
    $i = 0;
    while ($i < 7) {
        $date = strtotime("+$i days", $unixtime);
        $date_array = getdate($date);

        $weekdays .= "<td class=\"weekview_weekdays\">";

        // Mark today
        if ($date_array["year"] == $today_array["year"] &&
            $date_array["mon"] == $today_array["mon"] &&
            $date_array["mday"] == $today_array["mday"]) {
            $weekdays .= "<b><i>";
        }

        // Insert finnish day name
        $date_array = getdate($date);
        $wday = $date_array["wday"];
        $weekdays .= utility_get_finnish_weekday($wday);

        // Close today emphasis
        if ($date_array["year"] == $today_array["year"] &&
            $date_array["mon"] == $today_array["mon"] &&
            $date_array["mday"] == $today_array["mday"]) {
            $weekdays .= "</i></b>";
        }

        $weekdays .= "</td>\n";
        $i++;
    }

    $weekdays .= "</tr>\n";

    return $weekdays;
}

/**
 * Get previous & next week navigation links for a week view
 *
 * @param room The room under inspection
 * @param unixtime The monday of the current week
 */
function weekview_get_navigation_bar($room, $unixtime) {
    $bar = "<tr>\n";
    $bar .= "<td>&nbsp;</td>";

    // Previous week link
    $bar .= "<td class=\"weekview_week_link\">";
    $bar .= "<a href=\"weekview.php?room=$room&date=";
    $bar .= strtotime("-7 day", $unixtime)."\">";
    $bar .= "<img src=\"left.png\" border=\"0\" align=\"absmiddle\">";
    $week = date("W", $unixtime) - 1;
    if ($week < 1) {
        $week = 52;
    }
    $bar .= "<font class=\"weekview_week_link\"> Viikko $week</font></a>";
    $bar .= "</td>\n";

    // Current week number and month
    $bar .= "<td colspan=\"5\" class=\"weekview_current_week\">\n";

    // Week number
    $bar .= utility_get_finnish_week($unixtime);
    $bar .= ": ";

    // Month name
    $month = utility_get_finnish_month_name($unixtime);
    $bar .= $month;

    // In case month changes in the middle of the week, write also
    // the name of the next month
    $next_month = strtotime("+6day", $unixtime);
    $next_month = utility_get_finnish_month_name($next_month);
    if ($month != $next_month) {
        $bar .= " - " . $next_month;
    }
    $bar .= " " . date("Y", $unixtime);
    $bar .= "</td>\n";

    // Next week link
    $bar .= "<td class=\"weekview_week_link\">";
    $bar .= "<a href=\"weekview.php?room=$room&date=";
    $bar .= strtotime("+7 day", $unixtime)."\">";
    $week = date("W", $unixtime) + 1;
    if ($week > 52) {
        $week = 1;
    }
    $bar .= "<font class=\"weekview_week_link\">Viikko $week </font>";
    $bar .= "<img src=\"right.png\" border=\"0\" align=\"absmiddle\"></a>";
    $bar .= "</td>\n";

    $bar .= "</tr>\n";

    return $bar;
}

/**
 * Set all slots on the current hour free for all days of the week.
 *
 * @param unixtime Monday of current week at 00:00
 * @param current_hour The current hour (i.e. current row on the week view)
 * @param room The room ID under inspection
 * @param week_array Store the data to this week array
 * @param user The current user
 */
function weekview_set_all_slots_free($unixtime, $current_hour, $room, &$week_array, $user) {
    for ($i = 0; $i < 7; $i++) {
        $date = strtotime("+$i days $current_hour hours", $unixtime);
        $week_array[($i+1)%7] = weekview_get_free_slot_info($date, $room, $user);
    }
}

/**
 * Inspect one table row for reservations that occur during the given hour and
 * put its results to week_array to the day the reservation takes place on.
 *
 * @param reservation_row An array single reservation data
 */
function weekview_process_row($reservation_row, $current_hour, $user, &$week_array) {
	$date_array = getdate(strtotime($reservation_row["date"]));

    // Start is at or before current hour.
    // End is at or after current hour.
    if (($current_hour >= $reservation_row["start"]) && (($current_hour + 1) <= $reservation_row["end"])) {
        $own = FALSE;
		if ($user != null && $user->username == $reservation_row["reservee"]) {
			// This is current user's own reservation
		    $own = TRUE;
		}

        $week_array[$date_array["wday"]] = weekview_get_reservation_info($reservation_row["id"], $reservation_row["purpose"], $own);
	}
}

/**
 * Inspect one table row for weekly reservations that occur during the given
 * hour and put its results to week_array to the day the reservation takes
 * place on.
 */
function weekview_process_weekly($weekly_reservations, $current_hour, $unixtime, &$week_array) {
    foreach ($weekly_reservations as $row) {
        $weekday = $row["weekday"];
        $start_time = $row["start"];
        $end_time = $row["end"];
        $purpose = $row["purpose"];
        $weekly_id = $row["id"];

        // Clamp weekday to 0 - 6 (i.e. sunday - saturday)
        if ($weekday < 0) {
            $weekday = 0;
        } else if ($weekday > 6) {
            $weekday = 6;
        }

        if (($current_hour >= $start_time) && (($current_hour + 1) <= $end_time)) {
            // This is treated as someone else's reservation
            $week_array[$weekday] = weekview_get_weekly_info($weekly_id, $purpose);
        }
    }
}

/**
 * Get the control bar for the weekview component
 *
 * @param rooms All rooms
 * @param room The current room ID under inspection
 * @param unixtime The currently inspected time (for room selection)
 * @param user Current user info
 */
function weekview_get_control_bar($rooms, $room, $unixtime, $user) {
    $view = "<table rules=\"none\" width=\"800\" cellspacing=\"0\" cellpadding=\"0\">\n";
    $view .= "<tr>\n";

    // Room selection combo
    $view .= "<td align=\"left\" width=\"60%\" class=\"control_bar\">\n";
    $view .= "<form action=\"weekview.php\" method=\"GET\" name=\"RoomSelectionForm\">\n";
    $view .= "Tila/huone:\n";
    $view .= Room::get_selection_box($rooms, $room);
    $view .= "<input type=\"hidden\" name=\"date\" value=\"$unixtime\">\n";
    $view .= "<noscript><input type=\"submit\" value=\"Vaihda\" class=\"button\"></noscript>\n";
    $view .= "</form>\n";
    $view .= "</td>\n";

    if (!is_null($user) && !is_null($user->username)) {
        if ($user->admin) {
            // Room list button
            $view .= "<td class=\"control_bar\" align=\"left\">\n";
            $view .= "<form action=\"room_list.php\" method=\"post\" name=\"roomlist\">\n";
            $view .= "<input type=\"submit\" value=\"Huoneiden hallinta\" class=\"button\">\n";
            $view .= "</form>\n";
            $view .= "</td>\n";

            // User list button
            $view .= "<td class=\"control_bar\" align=\"left\">\n";
            $view .= "<form action=\"user_list.php\" method=\"post\" name=\"userlist\">\n";
            $view .= "<input type=\"submit\" value=\"K&auml;ytt&auml;jien hallinta\" class=\"button\">\n";
            $view .= "</form>\n";
            $view .= "</td>\n";
        }

        // Own info
        $view .= "<td align=\"right\" class=\"control_bar\">\n";
        $view .= "<form action=\"edit_user.php\" method=\"POST\">\n";
        $view .= "<input type=\"hidden\" name=\"username\" value=\"$user->username\">\n";
        $view .= "<input type=\"submit\" value=\"Omat tiedot\" class=\"button\">\n";
        $view .= "</form>\n";
        $view .= "</td>\n";
    }

    $view .= "</tr>\n";
    $view .= "</table>\n";

    return $view;
}

/**
 * Render the week view for the given room, on the given date, using the given
 * username as the current user.
 *
 * @param reservations An array of arrays of reservations for the current week
 * @param reservations_weekly An array of arrays of weekly reservations
 * @param unixtime Monday of the current week at 00:00
 * @param user The currently logged-in user's info
 * @param room The current room
 */
function weekview_get_view(array $reservations, array $weekly_reservations, $unixtime, $user, $room) {
    // Get the number of user reservations
    // Insert weekview table header
    $view = "<table rules=\"cols\" width=\"800\" cellpadding=\"1\" cellspacing=\"0\">\n";

    // Insert previous & next week navigation links
    $view .= weekview_get_navigation_bar($room, $unixtime);

    // Insert dates and day names to the week view
    $view .= weekview_get_weekdays($unixtime, $room);

    // This array holds one row (current hour) of reservations at a time.
    // As you can see, there are 7 slots in the array, one for each
    // day in a week.
    $week_array = array(0 => null, 1 => null, 2 => null, 3 => null, 4 => null, 5 => null, 6 => null);

    // Always go thru all available hours
    $current_hour = RESERVATION_FIRST_HOUR;
    while ($current_hour < RESERVATION_LAST_HOUR) {
        // By default, all hours are free
        weekview_set_all_slots_free($unixtime, $current_hour, $room, $week_array, $user);

        // Go thru all user reservation rows
        for ($row = 0; $row < count($reservations); $row++) {
            // Find all reservations happening during the current hour and put them to the week array
            weekview_process_row($reservations[$row], $current_hour, $user, $week_array);
		}

        // Fill in all weekly fixed reservations. This should happen AFTER user reservations have
        // been processed. If there are (for some unknown reason) overlapping user reservations,
        // weekly reservations always take precedence.
        weekview_process_weekly($weekly_reservations, $current_hour, $unixtime, $week_array);

        // Output one row of reservations (i.e. the current hour)
        $view .= "<tr bgcolor=\"#ffffff\">";

        // Hour numbers xx - yy
        $view .= sprintf("<td class=\"weekview_times\">%02d - %02d", $current_hour, ($current_hour + 1));

        // Data for each day of week for the current hour
        $view .= $week_array[1]; // monday
        $view .= $week_array[2]; // tuesday
        $view .= $week_array[3]; // wednesday
        $view .= $week_array[4]; // thursday
        $view .= $week_array[5]; // friday
        $view .= $week_array[6]; // saturday
        $view .= $week_array[0]; // sunday
        $view .= "</tr>\n";

        $current_hour++;
    }

    // Close the weekview table
    $view .= "</table>";

    return $view;
}

?>
